import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import Wrapper from '../hoc/Wrapper';
import {Helmet} from "react-helmet";
import { Redirect } from 'react-router-dom';
import { setRefIdentifier } from '../shared/utils';

class Ref extends React.Component {

  render() {

    if (this.props.match.params.ref_id !== undefined) {
      setRefIdentifier(this.props.match.params.ref_id);
    }

    return (
      <Wrapper>
        <Helmet>
          <meta property="og:url" content={this.props.location.pathname} />
        </Helmet>

        <Redirect to="/" />
      </Wrapper>
    );
  };
}

Ref.propTypes = {

}

Ref.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    loggedIn: state.security.loggedIn
  };
}

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Ref);
