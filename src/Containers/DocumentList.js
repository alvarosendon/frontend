import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import Wrapper from '../hoc/Wrapper';
import {Error} from '../Components/Error';
import NoDocuments from '../Components/Documents/NoDocuments'
import DocumentUpload from '../Components/Documents/DocumentUpload'
import Document from '../Components/Documents/Document';
import PrintForFreeModal from '../Components/Documents/PrintForFreeModal';
import DocumentDeleteConfirm from '../Components/Documents/DocumentDeleteConfirm';
import ReportProblem from '../Components/Documents/ReportProblem';
import DeliveryRequestModal from '../Components/Deliveries/DeliveryRequestModal';

import {
  fetchDocuments, toggleDocument, deleteDocument, reloadDocument, togglePrintForFree,
  toggleConfirmDelete, deliveryRequestToggle, deliveryRequestSelectAddress, fetchAddresses,
  documentReportProblemToggle, documentReportProblem
} from '../store/actions';

import { minimumPagesForDeliveries, maximumPagesForDeliveries } from '../shared/utils';

import './DocumentList.css'

class DocumentList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      problem: ""
    }

    this.onClickRefresh = this.onClickRefresh.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.onClickPrintForFree = this.onClickPrintForFree.bind(this);
    this.onClickDeliveryRequest = this.onClickDeliveryRequest.bind(this);
    this.onClickReportProblem = this.onClickReportProblem.bind(this);
    this.handleReportProblem = this.handleReportProblem.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentWillMount() {

    if (this.props.loggedIn === true) {
        this.props.fetchDocuments();
    }

  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.loggedIn === false && this.props.loggedIn === true) {
      this.props.fetchDocuments();
    }
  }

  handleDocumentReload = (documentId) => {
    const reloadId = setInterval (() => {
      this.props.reloadDocument(documentId, reloadId);
    }, 500);
  };

  onClickRefresh(e) {
    e.preventDefault();

    this.props.fetchDocuments();
  }

  onClickDelete(e) {
    e.preventDefault();

    if (this.props.selected.documents.lenght === 0) {
        return;
    }

    this.props.toggleConfirmDelete();
  }

  onClickPrintForFree(e) {
    e.preventDefault();

    this.props.togglePrintForFree();
  }

  onClickReportProblem(e, documentId) {
    e.preventDefault();

    this.setState({
      problem: ""
    });

    this.props.documentReportProblemToggle(documentId);
  }

  handleReportProblem(e) {
    e.preventDefault();

    this.props.documentReportProblem(this.state.problem);
  }

  handleDelete(e) {

    e.preventDefault();

    this.props.selected.documents.forEach(documentId => {
      this.props.deleteDocument(documentId, this.context.intl);
    });

  }

  onClickDeliveryRequest(e) {
    e.preventDefault();

    if (this.props.selected.pages < minimumPagesForDeliveries
      && this.props.selected.pages > 120) {
      return;
    }

    this.props.fetchAddresses();
    this.props.deliveryRequestToggle();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  getUserCredits() {

    let credits = 0;

    if (this.props.user && this.props.user.printCredits) {
      credits = this.props.user.printCredits;
    }

    return credits;
  }

  renderWarningMessageForCredits() {

    const credits = this.getUserCredits();

    if (this.props.selected.pages === 0 || this.props.selected.pages < credits) {
      return null;
    }

    return (
      <div className="row">
        <div className="col-sm-12">
          <Error errorMessage="Your credits are too low to get the selected documents printed." />
        </div>
      </div>
    );
  }

  renderWarningMessageForDeliveryPages() {

    const showWarning = this.props.selected.pages !== 0 &&
      (this.props.selected.pages <= (minimumPagesForDeliveries - 1) || this.props.selected.pages > maximumPagesForDeliveries)

    if (showWarning === false) {
      return null;
    }

    return (
      <div className="row">
        <div className="col-sm-12">
          <Error errorMessage={`The number of pages to request a delivery is between ${minimumPagesForDeliveries} and ${maximumPagesForDeliveries}.`} />
        </div>
      </div>
    );
  }

  buildDocuments(docs) {

    const credits = this.getUserCredits();
    const isDeliveryDisabled = this.props.selected.pages > credits || this.props.selected.pages <= (minimumPagesForDeliveries - 1);
    const isDeleteDisabled = this.props.selected.documents.length === 0;

    return (
      <Wrapper>
        <div className="row mt-2">
          <div className="col-md-12">
            <div className="document-list">
              <table className="table">
                <tbody>
                  {docs.map(d =>
                    <Document
                      key={d.id}
                      doc={d}
                      onSelect={() => this.props.toggleDocument(d.id, d.numberOfPages)}
                      onClickReportProblem={(e) => this.onClickReportProblem(e, d.id)}
                      />
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="row">

          <div className="col-md-12 d-none d-md-block">
            <button className="btn btn-primary mr-2"
              onClick={this.onClickDeliveryRequest}
              disabled={isDeliveryDisabled}>
              <FormattedMessage id="documents.btn.order" />
            </button>
            <button className="btn btn-primary" onClick={this.onClickPrintForFree}>
              <FormattedMessage id="documents.btn.print" />
            </button>
            <span className="text-muted ml-2">
              <FormattedMessage id="documents.selecteds" values={{"number": this.props.selected.documents.length}} />
            </span>
            <button className="btn btn-danger float-right" onClick={this.onClickDelete}
              disabled={isDeleteDisabled}>
                <FormattedMessage id="documents.btn.remove" />
              </button>
          </div>

          <div className="col-sm-12 d-md-none">
            <span className="text-muted">
              <FormattedMessage id="documents.selecteds" values={{"number": this.props.selected.documents.length}} />
            </span>
            <button className="btn btn-danger float-right" onClick={this.onClickDelete}
              disabled={isDeleteDisabled}>
                <FormattedMessage id="documents.btn.remove" />
              </button>
            <button className="btn btn-primary btn-block mt-4"
              onClick={this.onClickDeliveryRequest}
              disabled={isDeliveryDisabled}>
              <FormattedMessage id="documents.btn.order" />
            </button>
            <button className="btn btn-primary btn-block" onClick={this.onClickPrintForFree}>
              <FormattedMessage id="documents.btn.print" />
            </button>
          </div>

        </div>

        {this.renderWarningMessageForCredits()}
        {this.renderWarningMessageForDeliveryPages()}

        <PrintForFreeModal
          isOpen={this.props.openPrintForFree}
          toggle={this.props.togglePrintForFree}
          email={this.props.user?this.props.user.email:""}/>
        <DocumentDeleteConfirm
          isOpen={this.props.openDeleteConfirm}
          toggle={this.props.toggleConfirmDelete}
          onDelete={this.handleDelete} />
        <DeliveryRequestModal
          isOpen={this.props.deliveryRequestIsOpen}
          toggle={this.props.deliveryRequestToggle}
          onRequest={() => { console.log("On request delivery")}}
          onAddressSelection={this.props.deliveryRequestSelectAddress}
          addresses={this.props.addresses}
          selectedAddress={this.props.deliveryRequestAddress}
          />
        <ReportProblem
          isOpen={this.props.openReportProblem}
          toggle={this.props.documentReportProblemToggle}
          problem={this.state.problem}
          onChangeProblem={this.handleInputChange}
          onSubmit={this.handleReportProblem}
          intl={this.context.intl}
          />
      </Wrapper>
    );

  }

  buildErrorContent() {
    return (
      <div>
        <div>The documents cannot be loaded, please try again.</div>
        <button className="btn btn-sm btn-secondary" onClick={this.onClickRefresh}>Refresh</button>
      </div>
    );
  }

  buildEmptyMessage() {
    return (
      <div className="row mt-2">
        <div className="col-md-12">
          Your document list is empty. Upload your documents to print for free!!!
        </div>
      </div>
    );
  }

  renderLoadingMessage() {
    return (
      <div className="container-fluid">
        <div className="row mb-3">
          <div className="col">
            Loading your documents
          </div>
        </div>
      </div>
    );
  }

  renderAnonymous() {
    return (
      <Wrapper>
        <NoDocuments />
        <div className="row mt-5 mb-3 ml-3 mr-3">
          <div className="col">
            <DocumentUpload />
          </div>
        </div>
      </Wrapper>
    );
  }

  render() {

    if (this.props.loggedIn === false) {
      return this.renderAnonymous();
    }

    const docs = this.props.list;
    const loading = this.props.loading;

    if (loading) {
      return this.renderLoadingMessage();
    }

    let content;
    let title = null;

    if (docs.length === 0) {
      content = this.props.error?this.buildErrorContent():this.buildEmptyMessage();
    } else {
      content = this.buildDocuments(docs);
      title = (
        <div className="row">
          <div className="col">
            <h4 style={{"display": "inline-block"}}>
              <FormattedMessage id="documents.title" defaultMessage="Documents" />
            </h4>
            <span className="ml-2 mt-1 text-muted">
              <FormattedMessage id="documents.subtitle" defaultMessage="Documents" />
            </span>
          </div>
        </div>
      );
    }

    return (
        <div className="container-fluid">
          <div className="row mb-3">
            <div className="col">
              <DocumentUpload />
            </div>
          </div>
          {title}
          {content}
      </div>
    );
  };
}

DocumentList.propTypes = {
  list: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  openDeleteConfirm: PropTypes.bool.isRequired,
  openPrintForFree: PropTypes.bool.isRequired,
}

DocumentList.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    loggedIn: state.security.loggedIn,
    list: state.documents.list,
    loading: state.documents.loading,
    error: state.documents.error,
    openDeleteConfirm: state.documents.openDeleteConfirm,
    openPrintForFree: state.documents.openPrintForFree,
    openReportProblem: state.documents.openReportProblem,
    reportProblemId: state.documents.reportProblemId,
    selected: state.documents.selected,
    user: state.user.user,
    deliveryRequestIsOpen: state.documents.deliveryRequest.isOpen,
    deliveryRequestAddress: state.documents.deliveryRequest.selectedAddress,
    addresses: state.address.list
  };
}

const mapDispatchToProps = {
  fetchDocuments, toggleDocument, deleteDocument, reloadDocument, togglePrintForFree, toggleConfirmDelete,
  deliveryRequestToggle, deliveryRequestSelectAddress, fetchAddresses,
  documentReportProblemToggle, documentReportProblem
}

export default connect(mapStateToProps, mapDispatchToProps)(DocumentList);
