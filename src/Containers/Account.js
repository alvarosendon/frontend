import React from 'react';
import { connect } from "react-redux";
import { intlShape } from 'react-intl';
import AccountHeader from '../Components/AccountHeader';
import { userUpdate } from '../store/actions';
import AccountForm from '../Components/AccountForm';

import './Account.css'

class Account extends React.Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(model) {
      this.props.userUpdate(
        model.username,
        model.email,
        model.fullName,
        model.password,
        model.passwordRepeat,
        this.context.intl
      );
  }

  render () {

    return (
      <div className="container-fluid">

        <AccountHeader activeIndex={1} />

        <AccountForm intl={this.context.intl} onSubmit={this.handleSubmit}/>
      </div>

    );
  }

}

Account.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    "user": state.user.user
  };
}

const mapDispatchToProps = { userUpdate }

export default connect(mapStateToProps, mapDispatchToProps)(Account);
