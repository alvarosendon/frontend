import React from 'react';
import AccountHeader from '../Components/AccountHeader';
import api from '../Api';

import './Stats.css';

export default class Stats extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      prints: 0,
      pagesPrinted: 0,
      friendsRegistered: 0,
      friendsActivated: 0,
      friendsWithPrints: 0,
      creditsPerFriend: 0,
      creditsPerActivatedFriend: 0,
      creditsPerFriendWithPrints: 0
    }
  }

  componentWillMount() {
    api.get("/v1/user/stats", {headers: {'X-AUTH-TOKEN': localStorage.getItem("token")}})
    .then(response => {
      this.setState({
        prints: response.data['PRINTS'],
        pagesPrinted: response.data['PAGES_PRINTED'],
        friendsRegistered: response.data['FRIENDS_REGISTERED'],
        friendsActivated: response.data['FRIENDS_ACTIVATED'],
        friendsWithPrints: response.data['FRIENDS_WITH_PRINTS'],

        creditsPerFriend: response.data['CREDITS_PER_REGISTERED_FRIEND'],
        creditsPerActivatedFriend: response.data['CREDITS_PER_ACTIVATED_FRIEND'],
        creditsPerFriendWithPrints: response.data['CREDITS_PER_FRIENDS_WITH_PRINTS']
      });
    })
    .catch(error => {
      console.log(error);
    });
  }

  render () {

    return (
        <div className="container-fluid">
          <AccountHeader activeIndex={3}/>
          <div className="row mt-3">
            <div className="col-md-12">

              <h4>Invites</h4>

              <table className="table table-stats">
                <thead>
                  <tr>
                    <th>Status</th>
                    <th>People</th>
                    <th>Your reward</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Registered</td>
                    <td>{this.state.friendsRegistered}</td>
                    <td>{this.state.creditsPerFriend}</td>
                  </tr>
                  <tr>
                    <td>Activated</td>
                    <td>{this.state.friendsActivated}</td>
                    <td>{this.state.friendsActivated} x {this.state.creditsPerActivatedFriend} credits / week</td>
                  </tr>
                  <tr>
                    <td>Printed</td>
                    <td>{this.state.friendsWithPrints}</td>
                    <td>{this.state.friendsWithPrints} x {this.state.creditsPerFriendWithPrints} credits / week</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td>Total</td>
                    <td>{this.state.friendsRegistered}</td>
                    <td>{(this.state.friendsActivated * this.state.creditsPerActivatedFriend) + (this.state.friendsWithPrints * this.state.creditsPerFriendWithPrints)} credits / week</td>
                  </tr>
                </tfoot>
              </table>

              <h4>Prints</h4>
              You have printed {this.state.pagesPrinted} pages in {this.state.prints} prints for free.
            </div>
          </div>
        </div>
    );
  }
}
