import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import AddressItem from '../Components/Address/AddressItem';
import AccountHeader from '../Components/AccountHeader';
import AddressModalForm from '../Components/Address/AddressModalForm';
import { GlobalError } from '../Components/GlobalError';
import { submit } from 'redux-form';

import { fetchAddresses, addressDelete, addressToggleConfirmDelete, addressFormToggle, fetchAddress, saveAddress } from '../store/actions';

import './AddressList.css';

class AddressList extends React.PureComponent {

  constructor(props) {
    super(props);

    this.handleNewAddressClick = this.handleNewAddressClick.bind(this);
    this.handleEditAddressClick = this.handleEditAddressClick.bind(this);
    this.handleSaveAddressClick = this.handleSaveAddressClick.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  componentWillMount() {

    this.props.fetchAddresses();
  }

  handleResfresh() {
    this.componentWillMount();
  }

  handleNewAddressClick(e) {

    if (e !== undefined) {
      e.preventDefault();
    }

    this.props.addressFormToggle(true);
  }

  handleEditAddressClick(e, id) {
    if (e !== undefined) {
      e.preventDefault();
    }

    this.props.fetchAddress(id);
  }

  handleSaveAddressClick() {

    this.props.submit("addressForm");
  }

  handleOnSubmit(model) {

    this.props.saveAddress(model);
  }

  buildAddresses(addresses) {
    return (
      <div className="address-list">
        {addresses.map(address =>
          <AddressItem
            key={address.id}
            item={address}
            onDelete={this.props.addressDelete}
            isOpenConfirmDelete={this.props.openDeleteConfirm}
            toggleConfirmDelete={this.props.addressToggleConfirmDelete}
            onEdit={(e) => this.handleEditAddressClick(e, address.id)}
            />
        )}
      </div>
    );
  }

  buildEmptyMessage() {
    return (
      <div className="card zc-card">
        <div className="card-body text-center">
          <h4>
            <FormattedMessage id="address.list.empty.title"/>
          </h4>
          <FormattedMessage id="address.list.empty.description"/>
        </div>
      </div>
    );
  }

  render () {
    const addresses = this.props.list;

    let content;

    if (addresses.length === 0) {
      content = this.buildEmptyMessage();
    } else {
      content = this.buildAddresses(addresses);
    }

    return (
      <div className="container-fluid">
        <AccountHeader activeIndex={2}/>
        <div className="row mt-3">
          <div className="col-md-7">
            <GlobalError error={this.props.error}/>
            {content}
            <div>
              <a href="#newAddress" className="btn btn-secondary btn-md" onClick={this.handleNewAddressClick}>
                <FormattedMessage id="address.list.btn.add"/>
              </a>

              <AddressModalForm
                intl={this.context.intl}
                isOpen={this.props.openAddressForm}
                toggle={this.props.addressFormToggle}
                onSubmit={this.handleOnSubmit}
                onSaveClick={this.handleSaveAddressClick}
                isNew={this.props.isNew}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

}

AddressList.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    list: state.address.list,
    loading: state.address.loading,
    error: state.address.error,
    isNew: state.address.isNew,
    openDeleteConfirm: state.address.openDeleteConfirm,
    openAddressForm: state.address.openAddressForm
  };
}

const mapDispatchToProps = { fetchAddresses, addressDelete, addressToggleConfirmDelete, addressFormToggle, fetchAddress, saveAddress, submit }

export default connect(mapStateToProps, mapDispatchToProps)(AddressList);
