import * as actionTypes from './types';

export const fetchSections = () => {
  return {
    type: actionTypes.SECTIONS_FETCH
  };
};

export const fetchSectionsSuccess = (sections) => {
  return {
    type: actionTypes.SECTIONS_FETCH_SUCCESS,
    list: sections
  }
};

export const fetchSectionsError = (error) => {
  return {
    type: actionTypes.SECTIONS_FETCH_ERROR,
    error: error
  }
};

export const nextSection = () => {
  return {
    type: actionTypes.SECTIONS_NEXT
  }
};

export const toggleProfile = () => {
  return {
    type: actionTypes.PROFILE_TOGGLE_MODAL
  }
};

export const saveProfile = (sectionId, fieldsValues, finalStep) => {

  return {
    type: actionTypes.PROFILE_SAVE_SECTION,
    sectionId: sectionId,
    choices: fieldsValues,
    finalStep: finalStep
  }

};

export const saveProfileSuccess = () => {
  return {
    type: actionTypes.PROFILE_SAVE_SECTION_SUCCESS,
  }
};

export const saveProfileError = (error) => {
  return {
    type: actionTypes.PROFILE_SAVE_SECTION_ERROR,
    error: error
  }
};
