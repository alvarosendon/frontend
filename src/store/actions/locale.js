import * as actionTypes from './types';

export const localeSet = lang => ({
  type: actionTypes.LOCALE_SET,
  lang
});
