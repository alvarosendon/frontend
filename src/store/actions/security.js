import * as actionTypes from './types';

export const toggleLogIn = () => ({
  type: actionTypes.LOG_IN_TOGGLE
});

export const directLogIn = () => {
  return {
    type: actionTypes.LOGIN_AUTO
  }
};

export const logIn = (username, password) => {
  return {
    type: actionTypes.LOGIN,
    username: username,
    password: password
  }
};

export const logInSuccess = (data) => {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    token: data.token,
    user: data.user
  }
};

export const logInError = (error) => {
  return {
    type: actionTypes.LOGIN_ERROR,
    error: error
  }
};

export const logout = () => {
  return {
    type: actionTypes.LOGOUT
  }
};

export const logoutSuccess = () => {
  return {
    type: actionTypes.LOGOUT_SUCCESS
  }
};

export const logoutError = () => {
  return {
    type: actionTypes.LOGOUT_ERROR
  }
};
