import * as actionTypes from './types';

export const fetchDocuments = () => {
  return {
    type: actionTypes.DOCUMENTS_FETCH
  }
};

export const fetchDocumentsSuccess = (list) => {
  return {
    type: actionTypes.DOCUMENTS_FETCH_SUCCESS,
    list: list
  }
};

export const fetchDocumentsError = (error) => {
  return {
    type: actionTypes.DOCUMENTS_FETCH_ERROR,
    error: error
  }
};

export const toggleDocument = (documentId, pages) => {
    return {
        type: actionTypes.DOCUMENT_TOGGLE,
        documentId: documentId,
        pages: pages
    };
};

export const deleteDocument = (documentId, intl) => {
    return {
        type: actionTypes.DOCUMENT_DELETE,
        documentId: documentId,
        intl: intl
    };
};

export const deleteDocumentSuccess = () => {
    return {
        type: actionTypes.DOCUMENT_DELETE_SUCCESS
    };
};

export const deleteDocumentError = error => {
    return {
        type: actionTypes.DOCUMENT_DELETE_ERROR,
        error: error
    };
};

export const uploadDocumentSuccess = () => {
    return {
        type: actionTypes.DOCUMENT_UPLOAD_SUCCESS,
        uploading: false
    };
};

export const uploadDocument = (file) => {
    return {
        type: actionTypes.DOCUMENT_UPLOAD,
        file: file,
        uploading: true
    };
};

export const uploadProgress = progress => {
    return {
        type: actionTypes.DOCUMENT_UPLOAD_PROGRESS,
        progress: progress
    };
};

export const uploadDocumentError = error => {
    return {
        type: actionTypes.DOCUMENT_UPLOAD_ERROR,
        error: error,
        uploading: false
    };
};

export const reloadDocument = (document_id, reloadId) => {
    return {
        type: actionTypes.DOCUMENT_RELOAD,
        documentId: document_id,
        reloadId: reloadId
    };
};

export const reloadDocumentSuccess = (document, reloadId) => {
    return {
        type: actionTypes.DOCUMENT_RELOAD_SUCCESS,
        document: document,
        reloadId: reloadId
    };
};

export const reloadDocumentError = error => {
    return {
        type: actionTypes.DOCUMENT_RELOAD_ERROR,
        error: error
    };
};

export const toggleConfirmDelete = () => {

  return {
    type: actionTypes.DOCUMENT_TOGGLE_CONFIRM_DELETE,
  };
};

export const togglePrintForFree = () => {

  return {
    type: actionTypes.DOCUMENT_TOGGLE_PRINT_FOR_FREE,
  };
};

export const deliveryRequestToggle = () => {
  return {
    type: actionTypes.DELIVERY_REQUEST_TOGGLE
  }
};

export const deliveryRequestSelectAddress = (address) => {
  return {
    type: actionTypes.DELIVERY_REQUEST_SELECT_ADDRESS,
    address: address
  }
};

export const documentReportProblemToggle = (documentId) => {
  return {
    type: actionTypes.DOCUMENT_REPORT_PROBLEM_TOGGLE,
    documentId: documentId
  };
}

export const documentReportProblem = (problem) => {
  return {
    type: actionTypes.DOCUMENT_REPORT_PROBLEM,
    problem: problem
  };
}

export const documentReportProblemSuccess = () => {
  return {
    type: actionTypes.DOCUMENT_REPORT_PROBLEM_SUCCESS
  };
}

export const documentReportProblemError = (error) => {
  return {
    type: actionTypes.DOCUMENT_REPORT_PROBLEM_ERROR,
    error: error
  };
}
