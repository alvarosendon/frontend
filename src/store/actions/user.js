import * as actionTypes from './types';

export const fetchUser = () => {
  return {
    type: actionTypes.USER_FETCH
  }
};

export const fetchUserSuccess = (user) => {
  return {
    type: actionTypes.USER_FETCH_SUCCESS,
    user: user
  }
};

export const fetchUserError = (error) => {
  return {
    type: actionTypes.USER_FETCH_ERROR,
    error: error
  }
};

export const userUpdate = (username, email, fullname, newPassword, newPasswordRepeat, intl) => {
  return {
    type: actionTypes.USER_UPDATE,
    username: username,
    email: email,
    fullname: fullname,
    newPassword: newPassword,
    newPasswordRepeat: newPasswordRepeat,
    intl: intl
  };
};

export const userUpdateSuccess = () => {
  return {
    type: actionTypes.USER_UPDATE_SUCCESS
  };
};

export const userUpdateError = (error) => {
  return {
    type: actionTypes.USER_UPDATE_ERROR,
    error: error
  };
};

export const userFillPromoCode = (code, intl) => {
  return {
      type: actionTypes.USER_PROMO_CODE,
      code: code,
      intl: intl
  };
};

export const userFillPromoCodeSuccess = () => {
  return {
      type: actionTypes.USER_PROMO_CODE_SUCCESS
  };
};


export const userFillPromoCodeError = (error) => {
  return {
      type: actionTypes.USER_PROMO_CODE_ERROR,
      error: error
  };
};

export const userForgotPasswordToggle = () => {
  return {
      type: actionTypes.USER_FORGOT_PASSWORD_TOGGLE
  };
};

export const userForgotPassword = (email, intl) => {
  return {
      type: actionTypes.USER_FORGOT_PASSWORD,
      email: email,
      intl: intl
  };
};

export const userForgotPasswordSuccess = () => {
  return {
      type: actionTypes.USER_FORGOT_PASSWORD_SUCCESS
  };
};

export const userForgotPasswordError = (error) => {
  return {
      type: actionTypes.USER_FORGOT_PASSWORD_ERROR,
      error: error
  };
};

export const userCreateAccountToggle = () => {
  return {
      type: actionTypes.USER_CREATE_ACCOUNT_TOGGLE
  };
};

export const userCreateAccount = (email, password, passwordRepeat, intl) => {
  return {
      type: actionTypes.USER_CREATE_ACCOUNT,
      email: email,
      password: password,
      passwordRepeat: passwordRepeat,
      intl: intl
  };
};

export const userCreateAccountSuccess = () => {
  return {
      type: actionTypes.USER_CREATE_ACCOUNT_SUCCESS
  };
};

export const userCreateAccountError = (error) => {
  return {
      type: actionTypes.USER_CREATE_ACCOUNT_ERROR,
      error: error
  };
};

export const userInviteFriends = (friends, intl) => {
  return {
      type: actionTypes.USER_INVITE_FRIENDS,
      friends: friends,
      intl: intl
  };
};

export const userInviteFriendsSuccess = () => {
  return {
      type: actionTypes.USER_INVITE_FRIENDS_SUCCESS
  };
};

export const userInviteFriendsError = (error) => {
  return {
      type: actionTypes.USER_INVITE_FRIENDS_ERROR,
      error: error
  };
};

export const userCopyLink = (link, intl) => {
  return {
    type: actionTypes.USER_COPY_LINK,
    link: link,
    intl: intl
  }
}
