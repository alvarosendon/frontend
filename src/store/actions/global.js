import * as actionTypes from './types';

export const toggleAbout = () => {
  return {
    type: actionTypes.TOGGLE_ABOUT
  }
};

export const toggleContact = () => {
  return {
    type: actionTypes.TOGGLE_CONTACT
  }
};
