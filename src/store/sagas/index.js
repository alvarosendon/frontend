import { takeEvery, all } from "redux-saga/effects";

import * as actionTypes from "../actions/types";

import {
  adsFetchSaga
} from './ads';

import {
  documentsFetchSaga,
  documentDeleteSaga,
  documentUploadSaga,
  documentReloadSaga,
  documentReportProblemSaga
} from "./document";

import {
  userFetchSaga,
  userUpdateSaga,
  userPromoCodeSaga,
  userForgotPasswordSaga,
  userCreateAccountSaga,
  userInviteFriendsSaga,
  userCopyLinkSaga
} from "./user";

import {
  locationFetchCitiesSaga,
  locationFetchLocationsSaga
} from "./location";

import {
  logInSaga,
  logoutSaga
} from "./security";

import {
  addressesFetchSaga,
  addressFetchSaga,
  addressSaveSaga,
  addressDeleteSaga
} from "./address";

import {
  fetchSectionsSaga,
  saveSectionSaga
} from "./profile";

export function* watchDocument() {
    yield all([
        takeEvery(actionTypes.DOCUMENTS_FETCH, documentsFetchSaga),
        takeEvery(actionTypes.DOCUMENT_DELETE, documentDeleteSaga),
        takeEvery(actionTypes.DOCUMENT_UPLOAD, documentUploadSaga),
        takeEvery(actionTypes.DOCUMENT_RELOAD, documentReloadSaga),
        takeEvery(actionTypes.DOCUMENT_REPORT_PROBLEM, documentReportProblemSaga)
    ]);
}

export function* watchUser() {
    yield all([
        takeEvery(actionTypes.USER_FETCH, userFetchSaga),
        takeEvery(actionTypes.USER_UPDATE, userUpdateSaga),
        takeEvery(actionTypes.USER_PROMO_CODE, userPromoCodeSaga),
        takeEvery(actionTypes.USER_FORGOT_PASSWORD, userForgotPasswordSaga),
        takeEvery(actionTypes.USER_CREATE_ACCOUNT, userCreateAccountSaga),
        takeEvery(actionTypes.USER_INVITE_FRIENDS, userInviteFriendsSaga),
        takeEvery(actionTypes.USER_COPY_LINK, userCopyLinkSaga)
    ]);
}

export function* watchLocation() {
    yield all([
        takeEvery(actionTypes.CITIES_FETCH, locationFetchCitiesSaga),
        takeEvery(actionTypes.LOCATIONS_FETCH, locationFetchLocationsSaga)
    ]);
}

export function* watchSecurity() {
    yield all([
        takeEvery(actionTypes.LOGIN, logInSaga),
        takeEvery(actionTypes.LOGOUT, logoutSaga)
    ]);
}

export function* watchAddress() {
    yield all([
        takeEvery(actionTypes.ADDRESSES_FETCH, addressesFetchSaga),
        takeEvery(actionTypes.ADDRESS_FETCH, addressFetchSaga),
        takeEvery(actionTypes.ADDRESS_SAVE, addressSaveSaga),
        takeEvery(actionTypes.ADDRESS_DELETE, addressDeleteSaga)
    ]);
}

export function* watchProfile() {
    yield all([
        takeEvery(actionTypes.SECTIONS_FETCH, fetchSectionsSaga),
        takeEvery(actionTypes.PROFILE_SAVE_SECTION, saveSectionSaga)
    ]);
}

export function* watchAds() {
    yield all([
        takeEvery(actionTypes.ADS_FETCH, adsFetchSaga)
    ]);
}
