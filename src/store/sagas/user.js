import { put } from 'redux-saga/effects';
import api from '../../Api';
import copy from 'copy-to-clipboard';
import { getToken, globalErrorHandling, getRefIdentifier, clearRefIdentifier, API_SOURCE } from '../../shared/utils';
import {reset} from 'redux-form';
import { success as successNotification, error as errorNotification } from 'react-notification-system-redux';

import * as actions from '../actions';

export function* userFetchSaga() {
  try {
    const response = yield api.get("/v1/user", {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.fetchUserSuccess(response.data));
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.fetchUserError(errorObj));

    if (errorObj.errorCode === -2) {
        yield put(actions.logoutSuccess());
    }
  }
}

export function* userUpdateSaga(action) {

  const request = {
  	"username": action.username,
  	"email": action.email,
  	"fullname": action.fullname,
  	"newPassword": action.newPassword,
  	"newPasswordRepeat": action.newPasswordRepeat
  };

  try {
    yield api.post("/v1/user", request, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.userUpdateSuccess());
    yield put(actions.fetchUser());

    yield put(successNotification({
      message: action.intl.formatMessage({ id: "account.general.message.success"}),
      position: 'tc',
      autoDismiss: 4,
    }));

  } catch(error) {
    const errorObj = globalErrorHandling(error, "account.general.message.error");
    yield put(actions.userUpdateError(errorObj));

    yield put(errorNotification({
      message: action.intl.formatMessage({ id: errorObj.errorKey}),
      position: 'tc',
      autoDismiss: 4,
    }));
  }
}

export function* userPromoCodeSaga(action) {

  try {
    yield api.post("/v1/user/promo", {code: action.code}, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.userFillPromoCodeSuccess());
    yield put(reset("promoCodeForm"));
    yield put(actions.fetchUser());
    yield put(successNotification({
      message: action.intl.formatMessage({ id: "earnCredits.promo.message.success"}),
      position: 'tc',
      autoDismiss: 0,
    }));
  } catch(error) {

    const errorObj = globalErrorHandling(error, "earnCredits.promo.message.error");
    yield put(actions.userFillPromoCodeError(errorObj));

    yield put(errorNotification({
      message: action.intl.formatMessage({ id: errorObj.errorKey}),
      position: 'tc',
      autoDismiss: 0,
    }));
  }

}

export function* userForgotPasswordSaga(action) {

  try {
    yield api.post("/v1/forgot-password", {email: action.email});
    yield put(actions.userForgotPasswordSuccess());
    yield put(successNotification({
      message: action.intl.formatMessage({ id: "forgotPassword.message.success"}),
      position: 'tc',
      autoDismiss: 4,
    }));
  } catch(error) {

    const errorObj = globalErrorHandling(error, "forgotPassword.message.error");
    yield put(actions.userForgotPasswordError(errorObj));
  }
}

export function* userCreateAccountSaga(action) {

  let request = {
    source: API_SOURCE,
    agreePrivacyPolicy: true,
    email: action.email,
    password: action.password,
    passwordRepeat: action.passwordRepeat,

  };

  const ref_id = getRefIdentifier();
  if (ref_id !== undefined) {
    request.refId = ref_id;
  }

  try {
    yield api.post("/v1/registration", request);
    yield put(actions.userCreateAccountSuccess());
    yield put(successNotification({
      message: action.intl.formatMessage({ id: "createAccount.message.success"}),
      position: 'tc',
      autoDismiss: 4,
    }));
    clearRefIdentifier();
  } catch(error) {

    const errorObj = globalErrorHandling(error, "createAccount.message.error");
    yield put(actions.userCreateAccountError(errorObj));
  }
}

export function* userInviteFriendsSaga(action) {

  try {
    yield api.post("/v1/user/invite", {
      friends: action.friends,
    }, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.userInviteFriendsSuccess());
    yield put(reset("inviteFriendsByEmailForm"));
    yield put(successNotification({
      message: action.intl.formatMessage({ id: "inviteFriends.mailLink.message.success"}),
      position: 'tc',
      autoDismiss: 4,
    }));
  } catch(error) {

    const errorObj = globalErrorHandling(error, "inviteFriends.mailLink.message.error");
    yield put(actions.userInviteFriendsError(errorObj));
  }

}

export function* userCopyLinkSaga(action) {

  copy(action.link);
  yield put(successNotification({
    message: action.intl.formatMessage({ id: "inviteFriends.shareLink.message.success"}),
    position: 'tc',
    autoDismiss: 4,
  }));
}
