import { put } from 'redux-saga/effects';
import api from '../../Api';

import * as actions from '../actions';

export function* locationFetchCitiesSaga() {
  try {
    const response = yield api.get("/v1/cities");
    // Call user fetch success
    yield put(actions.fetchCitiesSuccess(response.data));
  } catch(error) {
    // Call user fetch error
    yield put(actions.fetchCitiesError(error));
  }
}

export function* locationFetchLocationsSaga() {
  try {

    /*let params = {};

    if (city !== undefined && city !== null) {
      params.cityId = city.id;
    }*/

    const response = yield api.get("/v1/locations");

    // Call user fetch success
    yield put(actions.fetchLocationsSuccess(response.data));
  } catch(error) {
    // Call user fetch error
    yield put(actions.fetchLocationsError(error));
  }
}
