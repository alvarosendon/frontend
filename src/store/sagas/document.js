import { put } from 'redux-saga/effects';
import api from "../../Api";
import { getToken, globalErrorHandling } from '../../shared/utils';
import { success as successNotification, error as errorNotification } from 'react-notification-system-redux';

import * as actions from "../actions";

export function* documentsFetchSaga() {
  try {
    const response = yield api.get("/v1/documents", {headers: {'X-AUTH-TOKEN': getToken()}});
    yield response.data.forEach(doc => {
      doc['loaded'] = (doc.status === 'IN_PROCESS') ? new Date() : null;
    });
    yield put(actions.fetchDocumentsSuccess(response.data));
  } catch(error) {
    yield put(actions.fetchDocumentsError(error));
  }
}

export function* documentDeleteSaga(action) {
    try {
        yield api.delete(`/v1/documents/${action.documentId}`, {headers: {'X-AUTH-TOKEN': getToken()}});
        yield put(actions.deleteDocumentSuccess());
        yield put(actions.fetchDocuments());

        yield put(successNotification({
          message: action.intl.formatMessage({ id: "documents.remove.message.success"}),
          position: 'tc',
          autoDismiss: 4,
        }));

    } catch (error) {
        const errorObj = globalErrorHandling(error, "documents.remove.message.error");
        yield put(actions.deleteDocumentError(errorObj));

        yield put(errorNotification({
          message: action.intl.formatMessage({ id: errorObj.errorKey}),
          position: 'tc',
          autoDismiss: 4,
        }));
    }
}

export function* documentUploadSaga(action) {
  try {
    const formData = new FormData();
    formData.append('document', action.file);
    let percentCompleted = 0;
    yield api.post('/v1/documents', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-AUTH-TOKEN':  getToken()
      },
      onUploadProgress: function(progressEvent) {
        percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        // console.log(percentCompleted);
        //yield put(actions.uploadDocumentProgress(percentCompleted));
      }
    });
    // yield put(actions.uploadDocumentProgress(percentCompleted));
    if(percentCompleted === 100) {
      console.log("% completed", percentCompleted);
      yield put(actions.uploadDocumentSuccess());
      yield put(actions.fetchDocuments());
    }
  } catch (error) {
    console.log("SAGA UPLOAD", error);
    yield put(actions.uploadDocumentError(error));
  }
}

export function* documentReloadSaga(action) {
    try {
        const response = yield api.get(`/v1/documents/${action.documentId}`, {headers: {'X-AUTH-TOKEN': getToken()}});

        if(response.data.status === 'IN_PROCESS') {
            response.data['reloadId'] = action.reloadId;
            yield put(actions.reloadDocumentSuccess(response.data));
        } else {
            clearInterval(action.reloadId);
            yield put(actions.fetchDocuments());
        }

    } catch (error) {
        console.log('RELOAD ERROR', error);
        yield put(actions.reloadDocumentError(error));
    }
}

export function* documentReportProblemSaga(action) {
    try {

        yield put(actions.documentReportProblemSuccess());

    } catch (error) {
      const errorObj = globalErrorHandling(error);
      yield put(actions.documentReportProblemError(errorObj));
    }
}
