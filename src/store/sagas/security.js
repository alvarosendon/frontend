import { put } from 'redux-saga/effects';
import api from '../../Api';
import { getToken, globalErrorHandling } from '../../shared/utils';

import * as actions from '../actions';

export function* logInSaga(action) {
  try {
    const response = yield api.post("/v1/login", {"username": action.username, "password": action.password});
    yield put(actions.logInSuccess(response.data));
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.logInError(errorObj));
  }
}

export function* logoutSaga() {

  try {
    yield api.post("/v1/logout", {}, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.logoutSuccess());
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.logoutError(errorObj));
  }
}
