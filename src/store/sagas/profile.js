import { put } from 'redux-saga/effects';
import api from '../../Api';
import { getToken, globalErrorHandling } from '../../shared/utils';

import * as actions from '../actions';

export function* fetchSectionsSaga () {

  try {
    const response = yield api.get("/v1/profile/sections", {headers: {'X-AUTH-TOKEN': getToken()}})
    yield put(actions.fetchSectionsSuccess(response.data));
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.fetchSectionsError(errorObj));
  }
}

export function* saveSectionSaga(action) {

  try {
    yield api.post(
      `/v1/profile/sections/${action.sectionId}/choices`,
      action.choices,
      {headers: {'X-AUTH-TOKEN': getToken()}}
    );
    yield put(actions.saveProfileSuccess());
    yield put(actions.fetchUser());

    if (action.finalStep === true) {
      yield put(actions.toggleProfile());
      yield put(actions.fetchSections());
    } else {
      yield put(actions.nextSection());
    }

  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.saveProfileError(errorObj));
  }
}
