import { put } from 'redux-saga/effects';
import api from '../../Api';
import { getToken, globalErrorHandling } from '../../shared/utils';

import * as actions from '../actions';

export function* addressesFetchSaga() {
  try {
    const response = yield api.get("/v1/user/addresses", {headers: {'X-AUTH-TOKEN': getToken()}});
    // Call user fetch success
    yield put(actions.fetchAddressesSuccess(response.data));
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.fetchAddressesError(errorObj));
  }
}

export function* addressFetchSaga(action) {
  try {
    const response = yield api.get("/v1/user/addresses/" + action.id, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.fetchAddressSuccess(response.data));
    yield put(actions.addressFormToggle());
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.fetchAddressError(errorObj));
  }
}

export function* addressSaveSaga(action) {

  let payload;
  const item = action.item;

  if (item.id) {
    payload = api.post("/v1/user/addresses/" + item.id, item, {headers: {'X-AUTH-TOKEN': getToken()}});
  } else {
    payload = api.put("/v1/user/addresses", item, {headers: {'X-AUTH-TOKEN': getToken()}});
  }

  try {
    const response = yield payload;
    yield put(actions.saveAddressSuccess(response.data));
    yield put(actions.fetchAddresses());
    yield put(actions.addressFormToggle());
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.saveAddressError(errorObj));
  }
}

export function* addressDeleteSaga(action) {

  try {
    yield api.delete("/v1/user/addresses/" + action.id, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.addressDeleteSuccess());
    yield put(actions.fetchAddresses());
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.addressDeleteError(errorObj));
  }
}
