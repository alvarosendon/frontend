import { put } from 'redux-saga/effects';
import api from '../../Api';
import { getToken, globalErrorHandling } from '../../shared/utils';

import * as actions from '../actions';

export function* adsFetchSaga () {

  try {
    const response = yield api.get("/v1/user/ads", {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.adsFetchSuccess(response.data));

    // Increase number of view for first ad
    if (response.data.length !== 0) {
      yield put(actions.adsIncreaseView(response.data[0].id));
    }

  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.adsFetchError(errorObj));
  }
}

export function* adsIncreaseViewSaga(action) {

  try {
    yield api.patch("/v1/user/ads/" + action.id, {headers: {'X-AUTH-TOKEN': getToken()}});
    yield put(actions.adsFetchSuccess());
  } catch(error) {
    const errorObj = globalErrorHandling(error);
    yield put(actions.adsIncreaseViewError(errorObj));
  }
}
