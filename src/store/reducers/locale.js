import { LOCALE_SET } from "../actions/types";

import messages from '../../i18n/messages';

export default function locale(state = { lang: "en", messages: messages["en"] }, action = {}) {
  switch (action.type) {
    case LOCALE_SET:
      localStorage.setItem("lang", action.lang);
      return {...state, lang: action.lang, messages: messages[action.lang]};
    default:
      return state;
  }
}
