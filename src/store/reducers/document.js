import * as actionTypes from '../actions/types';
import {updateObject} from '../../shared/utils';

const initialState = {
  list: [],
  files: [],
  uploading: false,
  loading: false,
  error: false,
  selected: {
    documents: [],
    pages: 0
  },
  openDeleteConfirm: false,
  openPrintForFree: false,
  openDetail: false,
  openReportProblem: false,
  reportProblemId: -1,
  deliveryRequest: {
    isOpen: false,
    selectedAddress: undefined
  }
};

const fetchDocuments = ( state, action ) => {
  return updateObject(state, {
    loading: true
  });
};

const fetchDocumentsSuccess = ( state, action ) => {
  return updateObject(state, {
    loading: false,
    list: action.list
  });
};

const fetchDocumentsError = ( state, action ) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const toggleDocument = (state, action) => {
  const index = state.selected.documents.indexOf(action.documentId);
  const newSelected = state.selected.documents.slice();
  let pages = state.selected.pages;
  if (index >= 0) {
    newSelected.splice(index, 1);
    pages -= action.pages;
  } else {
    newSelected.push(action.documentId);
    pages += action.pages;
  }

  return updateObject( state, {
    selected: {
      documents: newSelected,
      pages: pages
    }
  });
};

const deleteDocument = (state, action) => {
    return state;
};

const deleteDocumentSuccess = (state, action) => {
    return updateObject( state, {
        openDeleteConfirm: false,
        selected: {
            documents: [],
            pages: 0
        }
    })
};

const deleteDocumentError = (state, action) => {
    return updateObject( state, {
        error: action.error,
        openDeleteConfirm: false
    });
};

const uploadDocument = (state, action) => {
    return updateObject( state, {
        files: action.files,
        uploading: true
    });
};

const uploadDocumentProgress = (state, action) => {
    return updateObject( state, {
        progress: action.progress
    });
};

const uploadDocumentSuccess = (state, action) => {
    return updateObject( state, {
        uploading: false,
    });
};

const uploadDocumentError = (state, action) => {
    return updateObject( state, {
        uploading: false,
        files: []
    });
};

const reloadDocument = (state, action) => {
    return state;
};

const reloadDocumentSuccess = (state, action) => {
    const list = state.list;
    list[action.document.id] = action.document;
    return updateObject(state, {
        list: list
    });
};

const toggleConfirmDelete = (state, action) => {

  return updateObject(state, {
    openDeleteConfirm: !state.openDeleteConfirm
  });
};

const togglePrintForFree = (state, action) => {

  return updateObject(state, {
    openPrintForFree: !state.openPrintForFree
  });
};

const toggleDeliveryRequest = ( state, action ) => {
  return updateObject(state, {
    deliveryRequest: {...state.deliveryRequest, isOpen: !state.deliveryRequest.isOpen}
  });
};

const deliveryRequestSelectAddress = ( state, action ) => {
  return updateObject(state, {
    deliveryRequest: {...state.deliveryRequest, selectedAddress: action.address}
  });
};

const reportProblem = (state, action) => {

  return updateObject(state, {
    problem: action.problem
  });
};

const reportProblemSuccess = (state, action) => {
  return updateObject(state, {
    openReportProblem: false,
    problem: "",
    reportProblemId: -1
  });
};

const reportProblemError = (state, action) => {
  return updateObject(state, {
    openReportProblem: false,
    problem: "",
    reportProblemId: -1
  });
};

const reportProblemToggle = (state, action) => {
  return updateObject(state, {
    openReportProblem: !state.openReportProblem,
    reportProblemId: state.openReportProblem === true?-1:action.documentId,
    problem: ""
  });
};

const reducer = ( state = initialState, action ) => {
  switch(action.type) {
    case actionTypes.DOCUMENTS_FETCH: return fetchDocuments(state, action);
    case actionTypes.DOCUMENTS_FETCH_SUCCESS: return fetchDocumentsSuccess(state, action);
    case actionTypes.DOCUMENTS_FETCH_ERROR: return fetchDocumentsError(state, action);
    case actionTypes.DOCUMENT_TOGGLE: return toggleDocument(state, action);
    case actionTypes.DOCUMENT_DELETE: return deleteDocument(state, action);
    case actionTypes.DOCUMENT_DELETE_SUCCESS: return deleteDocumentSuccess(state, action);
    case actionTypes.DOCUMENT_DELETE_ERROR: return deleteDocumentError(state, action);
    case actionTypes.DOCUMENT_UPLOAD: return uploadDocument( state, action );
    case actionTypes.DOCUMENT_UPLOAD_PROGRESS: return uploadDocumentProgress( state, action );
    case actionTypes.DOCUMENT_UPLOAD_SUCCESS: return uploadDocumentSuccess( state, action );
    case actionTypes.DOCUMENT_UPLOAD_ERROR: return uploadDocumentError( state, action );
    case actionTypes.DOCUMENT_RELOAD: return reloadDocument( state, action );
    case actionTypes.DOCUMENT_RELOAD_SUCCESS: return reloadDocumentSuccess( state, action );
    case actionTypes.DOCUMENT_TOGGLE_CONFIRM_DELETE: return toggleConfirmDelete(state, action);
    case actionTypes.DOCUMENT_TOGGLE_PRINT_FOR_FREE: return togglePrintForFree(state, action);

    case actionTypes.DOCUMENT_REPORT_PROBLEM: return reportProblem(state, action);
    case actionTypes.DOCUMENT_REPORT_PROBLEM_SUCCESS: return reportProblemSuccess(state, action);
    case actionTypes.DOCUMENT_REPORT_PROBLEM_ERROR: return reportProblemError(state, action);
    case actionTypes.DOCUMENT_REPORT_PROBLEM_TOGGLE: return reportProblemToggle(state, action);

    case actionTypes.DELIVERY_REQUEST_TOGGLE: return toggleDeliveryRequest(state, action);
    case actionTypes.DELIVERY_REQUEST_SELECT_ADDRESS: return deliveryRequestSelectAddress(state, action);

    default: return state;
  }
};

export default reducer;
