import { updateObject } from '../../shared/utils';

import * as actionTypes from '../actions/types';

const initialProfileState = {
  list: [],
  loading: false,
  error: false,
  currentSection: -1,
  opened: false, // Modal is opened
  selectedChoices: []
}

const sectionsFetch = (state, action) => {
  return updateObject(state, {
    loading: true
  })
};

const sectionsFetchSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    list: action.list,
    currentSection: 0
  })
};

const sectionsFetchError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error,
    list: [],
    currentSection: -1
  })
};

/**
* Redux reducer for profile actions.
*/
export default function (state = initialProfileState, action) {
  switch (action.type) {
    case actionTypes.SECTIONS_FETCH: return sectionsFetch(state, action);
    case actionTypes.SECTIONS_FETCH_SUCCESS: return sectionsFetchSuccess(state, action);
    case actionTypes.SECTIONS_FETCH_ERROR: return sectionsFetchError(state, action);
    case actionTypes.SECTIONS_NEXT:
      return {...state, currentSection: state.currentSection + 1}
    case actionTypes.PROFILE_TOGGLE_MODAL:
      return {...state, opened: !state.opened}
    case actionTypes.PROFILE_SAVE_SECTION_SUCCESS:
      return state;
    case actionTypes.PROFILE_SAVE_SECTION_ERROR:
      return state;
    default:
      return state;
  }
};
