import * as actionTypes from '../actions/types';
import { setToken, clearToken, updateObject } from '../../shared/utils';

const securityState = {
  loading: false,
  error: false,
  loggedIn: false,
  logIn: {
    isOpen: false
  }
};

const login = (state, action) => {
  return updateObject(state, {
    loading: true
  });
}

const loginSuccess = (state, action) => {

  setToken(action.token);

  return updateObject(state, {
    loading: false,
    loggedIn: true,
    error: undefined,
    logIn: {...state.logIn, isOpen: false}
  });
}

const loginError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error,
    loggedIn: false
  });
}

/**
* Redux reducer for security actions.
*/
export default function (state = securityState, action) {
  switch (action.type) {
    case actionTypes.LOGIN: return login(state, action);
    case actionTypes.LOGIN_ERROR: return loginError(state, action);
    case actionTypes.LOGIN_SUCCESS: return loginSuccess(state, action);
    case actionTypes.LOGIN_AUTO:
      return {...state, loading: false , loggedIn: true, error: false};
    case actionTypes.LOGOUT_SUCCESS:
      clearToken();
      return {...state, loading: false, loggedIn: false};
    case actionTypes.LOG_IN_TOGGLE:
      return {...state, error: false, logIn: {isOpen: !state.logIn.isOpen}}
    default:
      return state;
  }
}
