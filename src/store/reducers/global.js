import * as actionTypes from '../actions/types';
import {updateObject} from '../../shared/utils';

const initialState = {
  openAbout: false,
  openContact: false,
  contactEmail: "support@zerocopy.be"
};

const toggleAbout = (state, action) => {

  return updateObject(state, {
    openAbout: !state.openAbout
  });
};

const toggleContact = (state, action) => {

  return updateObject(state, {
    openContact: !state.openContact
  });
};

const reducer = ( state = initialState, action ) => {
  switch(action.type) {
    case actionTypes.TOGGLE_ABOUT: return toggleAbout(state, action);
    case actionTypes.TOGGLE_CONTACT: return toggleContact(state, action);
    default: return state;
  }
};

export default reducer;
