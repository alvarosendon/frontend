import { updateObject } from '../../shared/utils';

import * as actionTypes from '../actions/types';

const initialState = {
  list: [],
  loading: false
}

const adsFetch = (state, action) => {
  return updateObject(state, {
    loading: true,
    error: undefined
  })
};

const adsFetchSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    list: action.ads
  })
};

const adsFetchError = (state, action) => {
  return updateObject(state, {
    loading: false,
    list: [],
    error: action.error
  })
};

const adsIncreaseView = (state, action) => {
  return updateObject(state, {
  })
};

const adsIncreaseViewSuccess = (state, action) => {
  return updateObject(state, {
  })
};
const adsIncreaseViewError = (state, action) => {
  return updateObject(state, {
    error: action.error
  })
};

/**
* Redux reducer for ads actions.
*/
export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADS_FETCH: return adsFetch(state, action);
    case actionTypes.ADS_FETCH_SUCCESS: return adsFetchSuccess(state, action);
    case actionTypes.ADS_FETCH_ERROR: return adsFetchError(state, action);
    case actionTypes.ADS_INCREASE_VIEW: return adsIncreaseView(state, action);
    case actionTypes.ADS_INCREASE_VIEW_SUCCESS: return adsIncreaseViewSuccess(state, action);
    case actionTypes.ADS_INCREASE_VIEW_ERROR: return adsIncreaseViewError(state, action);
    default:
      return state;
  }
};
