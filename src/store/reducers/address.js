import { updateObject } from '../../shared/utils';
import * as actionTypes from '../actions/types';

const emptyAddress = {
  name: "",
  surname: "",
  street: "",
  streetNumber: "",
  boxNumber: "",
  postalCode: "",
  city: ""
};

const initialAddressState = {
  list: [],
  loading: false,
  error: undefined,
  addressSaved: false,
  item: emptyAddress,
  openDeleteConfirm: false,
  openAddressForm: false,
  isNew: false
};

const addressFetchSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    item: action.item
  });
}

const addressDelete = (state, action) => {
  return updateObject(state, {
    loading: true,
    openDeleteConfirm: false
  });
};

const addressDeleteSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
  });
};

const addressDeleteError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const addressToggleConfirmDelete = (state, action) => {
  return updateObject(state, {
    openDeleteConfirm: !state.openDeleteConfirm
  });
};

const addressFormToggle = (state, action) => {

  if (action.isNew === true) {
    return updateObject(state, {
      openAddressForm: !state.openAddressForm,
      item: emptyAddress,
      isNew: true
    });
  }

  return updateObject(state, {
    openAddressForm: !state.openAddressForm,
    isNew: false
  });
};

/**
* Redux reducer for addresses actions.
*/
export default function (state = initialAddressState, action) {
  switch (action.type) {
    case actionTypes.ADDRESSES_FETCH_ERROR:
    case actionTypes.ADDRESS_FETCH_ERROR:
    case actionTypes.ADDRESS_SAVE_ERROR:
      return {...state, loading: false, error: action.error};
    case actionTypes.ADDRESSES_FETCH_SUCCESS:
      return {...state, loading: false, list: action.list};
    case actionTypes.ADDRESS_FETCH_SUCCESS: return addressFetchSuccess(state, action);
    case actionTypes.ADDRESS_SAVE_SUCCESS:
      return {...state, loading: false, isNew: false, addressSaved: true};
    case actionTypes.ADDRESS_DELETE: return addressDelete(state, action);
    case actionTypes.ADDRESS_DELETE_SUCCESS: return addressDeleteSuccess(state, action);
    case actionTypes.ADDRESS_DELETE_ERROR: return addressDeleteError(state, action);
    case actionTypes.ADDRESS_TOGGLE_CONFIRM_DELETE: return addressToggleConfirmDelete(state, action);
    case actionTypes.ADDRESS_FORM_TOGGLE: return addressFormToggle(state, action);
    default:
      return state;
  }
}
