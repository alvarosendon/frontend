import * as actionTypes from '../actions/types';
import { updateObject } from '../../shared/utils';

const initialUserState = {
  loading: false,
  error: null,
  user: undefined,
  forgotPassword: {
    open: false,
    loading: false,
    error: undefined
  },
  createAccount: {
    open: false,
    loading: false,
    error: undefined
  }
};

const fetchUser = (state, action) => {
  return updateObject(state, {
    loading: true
  });
};

const fetchUserSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    user: action.user
  });
};

const fetchUserError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const userUpdate = (state, action) => {
  return updateObject(state, {
    loading: true
  });
}

const userUpdateSuccess = (state, action) => {
  return updateObject(state, {
    loading: false
  });
}

const userUpdateError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
}

const userPromoCode = (state, action) => {
  return updateObject(state, {
    loading: true
  });
}

const userPromoCodeSuccess = (state, action) => {
  return updateObject(state, {
    loading: false
  });
}

const userPromoCodeError = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
}

const userForgotPassword = (state, action) => {
  return updateObject(state, {
    forgotPassword: {...state.forgotPassword, loading: true}
  });
}

const userForgotPasswordSuccess = (state, action) => {
  return updateObject(state, {
    forgotPassword: {...state.forgotPassword, loading: false, open: false}
  });
}

const userForgotPasswordError = (state, action) => {
  return updateObject(state, {
    forgotPassword: {...state.forgotPassword, loading: false, error: action.error }
  });
}

const userForgotPasswordToggle = (state, action) => {
  return updateObject(state, {
    forgotPassword: {...state.forgotPassword, open: !state.forgotPassword.open, error: undefined }
  });
}

const userCreateAccount = (state, action) => {
  return updateObject(state, {
    createAccount: {...state.createAccount, loading: true }
  });
}

const userCreateAccountSuccess = (state, action) => {
  return updateObject(state, {
    createAccount: {...state.createAccount, loading: false, open: false }
  });
}

const userCreateAccountError = (state, action) => {
  return updateObject(state, {
    createAccount: {...state.createAccount, loading: false, error: action.error }
  });
}

const userCreateAccountToggle = (state, action) => {
  return updateObject(state, {
    createAccount: {...state.createAccount, open: !state.createAccount.open, error: undefined }
  });
}

/**
* Redux reducer for user actions.
*/
export default function (state = initialUserState, action) {
  switch (action.type) {
    case actionTypes.USER_FETCH: return fetchUser(state, action);
    case actionTypes.USER_FETCH_SUCCESS: return fetchUserSuccess(state, action);
    case actionTypes.USER_FETCH_ERROR: return fetchUserError(state, action);

    case actionTypes.USER_UPDATE: return userUpdate(state, action);
    case actionTypes.USER_UPDATE_SUCCESS: return userUpdateSuccess(state, action);
    case actionTypes.USER_UPDATE_ERROR: return userUpdateError(state, action);

    case actionTypes.USER_PROMO_CODE: return userPromoCode(state, action);
    case actionTypes.USER_PROMO_CODE_SUCCESS: return userPromoCodeSuccess(state, action);
    case actionTypes.USER_PROMO_CODE_ERROR: return userPromoCodeError(state, action);

    case actionTypes.USER_FORGOT_PASSWORD: return userForgotPassword(state, action);
    case actionTypes.USER_FORGOT_PASSWORD_SUCCESS: return userForgotPasswordSuccess(state, action);
    case actionTypes.USER_FORGOT_PASSWORD_ERROR: return userForgotPasswordError(state, action);
    case actionTypes.USER_FORGOT_PASSWORD_TOGGLE: return userForgotPasswordToggle(state, action);

    case actionTypes.USER_CREATE_ACCOUNT: return userCreateAccount(state, action);
    case actionTypes.USER_CREATE_ACCOUNT_SUCCESS: return userCreateAccountSuccess(state, action);
    case actionTypes.USER_CREATE_ACCOUNT_ERROR: return userCreateAccountError(state, action);
    case actionTypes.USER_CREATE_ACCOUNT_TOGGLE: return userCreateAccountToggle(state, action);

    default: return state;
  }
};
