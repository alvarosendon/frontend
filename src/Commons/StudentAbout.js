import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import { Carousel, CarouselItem } from 'reactstrap';
import StudentBox from "./StudentBox";
import "./StudentAbout.css";
import Wrapper from '../hoc/Wrapper';
import { studentAboutInterval } from '../shared/utils';

class StudentAbout extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
    };

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 2 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? 2 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  render() {

    if (this.props.loggedIn === true) {
      return null;
    }

    const items = [
      (<Wrapper>
        <StudentBox
        position="left"
        img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/baptiste.png?v9"
        name="Baptiste Carpentier"
        comment="Why pay for something that can be for free?"
        course="Master Civil Engineering"
        university="Ghent University"
        />

        <StudentBox
          position="right"
          img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/fikre.png?v9"
          name="Fikre Teklu"
          comment="I am a frequent Zerocopy user because I have to print every week. Thanks!"
          course="Master Civil Engineering"
          university="Ugent"
        />
      </Wrapper>),
      (<Wrapper>
        <StudentBox
          position="left"
          img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/sienie_van_geerteruy.png?v9"
          name="Sienie Van Geerteruy"
          comment="As a hardworking student I frequently use Zerocopy."
          course="Taal & Letterkunde"
          university="Ugent"
          />

        <StudentBox
          position="right"
          img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/stephanie_b.png?v9"
          name="Stephanie B."
          comment="Zerocopy is a great concept for students! Actions to get more prints are worth your time."
          course="Master Bedrijfskunde"
          university="Ugent"
          />
      </Wrapper>),
      (<Wrapper>
        <StudentBox
          position="left"
          img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/jade_liu.png?v9"
          name="Jade Liu"
          comment="Zerocopy is very quick and easy!"
          course="2nd Bachelor Sinology"
          university="KU Leuven"
          />

        <StudentBox
          position="right"
          img="https://zerocopy.be/bundles/zerocopymain/upload/img/testimonials/sofie_duym.png?v9"
          name="Sofie Duym"
          comment="Thank you very much for reducing the costs of study. It's so simple: register and you're good to go!"
          course="1st Bachelor Communication"
          university="Ghent University"
          />
      </Wrapper>)
    ];

    const slides = items.map((item, index) => {
      return (
        <CarouselItem key={index}>
          {item}
        </CarouselItem>
      );
    });

    return (
      <div>
        <strong><FormattedMessage id="aboutUs.students.title" /></strong>

        <div className="student-comments">

          <Carousel activeIndex={this.state.activeIndex} next={this.next} previous={this.previous}
            interval={studentAboutInterval} pause={false} ride="carousel">

            {slides}

          </Carousel>

        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    "loggedIn": state.security.loggedIn
  };
}

const mapDispatchToProps = { }

export default connect(mapStateToProps, mapDispatchToProps)(StudentAbout);
