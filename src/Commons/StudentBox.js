import React from 'react';
import PropTypes from 'prop-types';

class StudentBox extends React.PureComponent {

  render() {

    const img = (
      <div className={this.props.position === "left"?"col-2 p-0 pl-4":"col-2 p-0"}>
        <img src={this.props.img} alt="Student icon" />
      </div>
    );

    return (
      <div className="row mt-3">

        {this.props.position === "left"?img:null}

        <div className="col-9">
          <div className={"pl-2 pt-2 pb-2 pr-2 bubble bubble-" + this.props.position}>
            {this.props.comment}
          </div>
          <div className={"text-" + this.props.position}>
            <span className="name">{this.props.name}</span>&nbsp;
            <span className="text-secondary">{this.props.course}, {this.props.university}</span>
          </div>
        </div>

        {this.props.position === "right"?img:null}

      </div>
    );
  }
}

StudentBox.propTypes = {
  position: PropTypes.oneOf(['left', 'right']),
  img: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  course: PropTypes.string.isRequired,
  university: PropTypes.string.isRequired
};

export default StudentBox;
