import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import "./MediaAbout.css";

class MediaAbout extends React.Component {

  render() {

    if (this.props.loggedIn === true) {
      return null;
    }

    return (
      <div className="media-comments">
        <strong><FormattedMessage id="aboutUs.media.title" /></strong>

        <div className="mt-3">
          <a target="_blank" rel="noopener noreferrer" href="https://www.tijd.be/partnercontent/ing/partner-voor-innovatie/Onze-bankier-belde-en-zei-Ik-heb-nog-extra-kapitaal-gevonden-voor-jullie/9942028">
            <img src="https://test.zerocopy.be/bundles/zerocopymain/upload/img/content/detijd.png?v9"
              style={{"maxWidth": "150px"}} alt="DETIJD"/>
          </a>
          <a target="_blank" rel="noopener noreferrer" href="https://www.lecho.be/partnercontent/ing/partenaire-en-innovation/Notre-banquier-nous-a-appeles-pour-nous-dire-j-ai-encore-trouve-de-l-argent-pour-vous/9942031"
            className="ml-2">
            <img src="https://test.zerocopy.be/bundles/zerocopymain/upload/img/content/lecho.png?v9"
              style={{"maxWidth": "150px"}} alt="L'echo"/>
          </a>
          <a target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=FCP5AnXm_j4"
            className="ml-3">
            <img src="https://test.zerocopy.be/bundles/zerocopymain/upload/img/content/vtm.png?v9"
              style={{"maxWidth": "150px"}} alt="VTM"/>
          </a>
          <a target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=IwFL5AQGWJo"
            className="ml-3">
            <img src="https://test.zerocopy.be/bundles/zerocopymain/upload/img/content/rtl.png?v9"
              style={{"maxWidth": "150px"}} alt="RTL"/>
          </a>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    "loggedIn": state.security.loggedIn
  };
}

const mapDispatchToProps = { }

export default connect(mapStateToProps, mapDispatchToProps)(MediaAbout);
