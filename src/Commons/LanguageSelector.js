import React from 'react';
import { connect } from "react-redux";
import { localeSet } from "../store/actions";

import './LanguageSelector.css';

class LanguageSelector extends React.Component {

  onLanguageItemClick(newLang) {

    if (newLang !== this.props.activeLang) {
        this.props.onChangeLocale(newLang);
    }
  }

  languageItemClass(itemLang) {
    return this.props.activeLang === itemLang?"zerocopy-link active":"zerocopy-link";
  }

  render () {

    return (

      <ul className="list-inline">
        <li className="list-inline-item">
          <a href="#LanguageEN" className={this.languageItemClass('en')}
            onClick={e => {e.preventDefault();this.onLanguageItemClick("en")}}>EN</a>
        </li>
        <li className="list-inline-item">
          <a href="#LanguageNL" className={this.languageItemClass('nl')}
            onClick={e => {e.preventDefault();this.onLanguageItemClick("nl")}}>NL</a>
        </li>
        <li className="list-inline-item">
          <a href="#LanguageFR" className={this.languageItemClass('fr')}
            onClick={e => {e.preventDefault();this.onLanguageItemClick("fr")}}>FR</a>
        </li>
        <li className="list-inline-item">
          <a href="#LanguageES" className={this.languageItemClass('es')}
            onClick={e => {e.preventDefault();this.onLanguageItemClick("es")}}>ES</a>
        </li>
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeLang: state.locale.lang
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onChangeLocale: lang => {
      dispatch(localeSet(lang))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSelector);
