import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import LanguageSelector from './LanguageSelector';
import About from '../Components/About';
import Contact from '../Components/Contact';

import {
  toggleAbout, toggleContact
} from '../store/actions';

import './Footer.css';

class Footer extends React.Component {

  constructor(props) {
    super(props);

    this.onClickAbout = this.onClickAbout.bind(this);
    this.onClickContact = this.onClickContact.bind(this);
  }

  onClickAbout(e) {
    e.preventDefault();

    this.props.toggleAbout();
  }

  onClickContact(e) {
    e.preventDefault();

    this.props.toggleContact();
  }

  render () {

    return (
      <footer className="pl-3 pr-3">
        <div className="mt-2">
          <a href="#About" target="_blank" className="mr-3 zerocopy-link" rel="noopener noreferrer"
            onClick={this.onClickAbout}>
            <FormattedMessage id="footer.about" />
          </a>
          <a href="http://www.zerocopyjobs.com/" target="_blank" className="mr-3 zerocopy-link" rel="noopener noreferrer">
            <FormattedMessage id="footer.jobs" />
          </a>
          <a href="#Contact" target="_blank" className="mr-3 zerocopy-link" rel="noopener noreferrer"
            onClick={this.onClickContact}>
            <FormattedMessage id="footer.contact" />
          </a>
          <a href="https://blog.zerocopy.be/faq/" target="_blank" className="mr-3 zerocopy-link" rel="noopener noreferrer">
            <FormattedMessage id="footer.faq" />
          </a>
          <a href="https://blog.zerocopy.be" target="_blank" className="zerocopy-link" rel="noopener noreferrer">
            <FormattedMessage id="footer.blog" />
          </a>

          <a href="https://twitter.com/Zerocopy" target="_blank" rel="noopener noreferrer" className="float-right footer-sm-link">
            <i className="fab fa-twitter" aria-hidden="true"></i> Twitter
          </a>
          <a href="https://www.facebook.com/Zerocopy.be" target="_blank" rel="noopener noreferrer" className="mr-3 float-right footer-sm-link">
            <i className="fab fa-facebook-f" aria-hidden="true"></i> Facebook
          </a>
        </div>

        <div className="mt-2">
          <div className="float-left text-muted">
            <FormattedMessage id="footer.copyright" values={{year: new Date().getFullYear()}} />
          </div>
          <div className="float-right">
            <LanguageSelector />
          </div>
        </div>

        <About isOpen={this.props.openAbout} toggle={this.props.toggleAbout} contactEmail={this.props.contactEmail} />
        <Contact isOpen={this.props.openContact} toggle={this.props.toggleContact} contactEmail={this.props.contactEmail} />
      </footer>
    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    openAbout: state.global.openAbout,
    openContact: state.global.openContact,
    contactEmail: state.global.contactEmail
  };
}

const mapDispatchToProps = {
  toggleAbout, toggleContact
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
