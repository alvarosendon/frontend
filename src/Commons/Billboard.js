import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Carousel, CarouselItem } from 'reactstrap';

import { adsFetch, adsIncreaseView } from '../store/actions';

import './Billboard.css';

class Billboard extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
    };

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.onEnter = this.onEnter.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.show === false && this.props.show === true) {
      this.props.adsFetch();
    }
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  onEnter() {

    const currentId = this.props.ads[this.state.activeIndex].id;

    this.props.adsIncreaseView(currentId);
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.props.ads.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.props.ads.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  render () {

    if (this.props.show === false || this.props.ads.length === 0) {
      return null;
    }

    const { activeIndex } = this.state;
    const { ads } = this.props;

    const slides = ads.map((ad) => {
      return (
        <CarouselItem onExiting={this.onExiting} onExited={this.onExited} onEnter={this.onEnter} key={ad.src}
          className="text-center">
          <a href={ad.clickUrl} target="_blank">
            <img src={ad.src} alt={ad.altText} style={{"maxWidth": "100%", "height": "250px"}} />
          </a>
        </CarouselItem>
      );
    });

    const carouselStyle = {
      "maxWidth": "950px",
      "maxHeight": "250px"
    };

    return (
      <div className="billboard">
        <div className="m-auto" style={carouselStyle}>
          <Carousel activeIndex={activeIndex} next={this.next} previous={this.previous} interval={10000}
            pause={false} ride="carousel">
            {slides}
          </Carousel>
        </div>
      </div>
    );
  }

}

Billboard.propTypes = {
  show: PropTypes.bool.isRequired
}

Billboard.contextTypes = {
    mixpanel: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    ads: state.ads.list
  };
}

const mapDispatchToProps = { adsFetch, adsIncreaseView }

export default connect(mapStateToProps, mapDispatchToProps)(Billboard);
