export const API_SOURCE = "WebReact";
export const defaultMaxLength = 255;
export const minimumPagesForDeliveries = 55;
export const maximumPagesForDeliveries = 120;
export const numberOfDaysToDelete = 10;
export const studentAboutInterval= 10000;

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const globalErrorHandling = (error, defaultErrorKey) => {

  if (error.response && error.response.data && error.response.data.errorCode) {

    return {
      errorCode: error.response.data.errorCode,
      errorKey: "errors.api.code." + error.response.data.errorCode
    };

  } else if (error.response && error.response.status && error.response.status === 401) {
    clearToken();
    return {"errorCode": -2, "errorKey": "errors.unauthorized"};
  } else if (!error.status || error.message === "Network Error") {
    return {"errorCode": -1, "errorKey": "errors.network"};
  } else {
    return {"errorCode": 0, "errorKey": defaultErrorKey};
  }
};

export const calendarStrings = {
    lastDay : 'DD/MM/YYYY HH:mm',
    sameDay : '[Today at] HH:mm',
    nextDay : 'DD/MM/YYYY HH:mm',
    lastWeek : 'DD/MM/YYYY HH:mm',
    sameElse : 'DD/MM/YYYY HH:mm'
};

export const setToken = (token, rememberMe) => {

  if (rememberMe === true) {
    sessionStorage.setItem("token", token)
  } else {
    localStorage.setItem("token", token)
  }
};

export const getToken = () => {

  const sessionToken = sessionStorage.getItem("token");

  if (sessionToken !== undefined && sessionToken !== null) {
    return sessionToken;
  }

  return localStorage.getItem("token");
};

export const clearToken = () => {
  localStorage.removeItem("token");
  sessionStorage.removeItem("token");
}

export const setRefIdentifier = id => {
  sessionStorage.setItem("ref_id", id);
}

export const clearRefIdentifier = () => {
  sessionStorage.removeItem("ref_id");
}

export const getRefIdentifier = () => {
  return sessionStorage.getItem("ref_id");
}
