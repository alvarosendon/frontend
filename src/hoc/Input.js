import React from 'react';
import { FormattedMessage } from 'react-intl';
import SimpleInput from './SimpleInput';

const input = ({
  input, id, label, placeholder, className, type, maxLength, autoComplete, inputRef,
  meta: { touched, error } }) => {

    if (touched && error && className !== "") {
        className += " is-invalid";
    }

  return (
    <div className="form-group">
      <label className="font-weight-bold" htmlFor={id}>{label}</label>
      <input {...input}
        id={id}
        className={className}
        placeholder={placeholder}
        type={type}
        autoComplete={autoComplete}
        maxLength={maxLength}
        ref={inputRef}
      />
      {touched && (error && <div className="text-danger"><FormattedMessage id={error.id} values={error.values} /></div>)}
    </div>
  );
}

export default input;
