import React from 'react';
import { FormattedMessage } from 'react-intl';
import SimpleInput from './SimpleInput';

const textarea = ({
  input, id, label, placeholder, className, inputRef,
  meta: { touched, error } }) => {

    if (touched && error && className !== "") {
        className += " is-invalid";
    }

  return (
    <div className="form-group">
      <label className="font-weight-bold" htmlFor={id}>{label}</label>
      <textarea {...input}
        id={id}
        className={className}
        placeholder={placeholder}
        ref={inputRef}
      />
      {touched && (error && <div className="text-danger"><FormattedMessage id={error.id} values={error.values} /></div>)}
    </div>
  );
}

export default textarea;
