import React from 'react';
import { FormattedMessage } from 'react-intl';
import Wrapper from './Wrapper';

const simpleInput = ({
  input, id, label, placeholder, className, type, maxLength, autoComplete, inputRef,
  meta: { touched, error } }) => {

    if (touched && error && className !== "") {
        className += " is-invalid";
    }

    return (
      <Wrapper>
        <input
          {...input}
          id={id}
          className={className}
          placeholder={placeholder}
          type={type}
          maxLength={maxLength}
          autoComplete={autoComplete}
          ref={inputRef}
        />
        {touched && (error && <div className="text-danger"><FormattedMessage id={error.id} values={error.values} /></div>)}
      </Wrapper>
    );
}

export default simpleInput;
