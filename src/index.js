import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import { addLocaleData } from 'react-intl';

import mixpanel from 'mixpanel-browser';
import MixpanelProvider from 'react-mixpanel';

import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import fr from 'react-intl/locale-data/fr';
import nl from 'react-intl/locale-data/nl';
import { localeSet } from "./store/actions";

import rootReducer from "./store/reducers";
import {
  watchUser, watchDocument, watchLocation, watchSecurity,
  watchAddress, watchProfile, watchAds
} from "./store/sagas";

import App from './App';
import registerServiceWorker from './registerServiceWorker';

addLocaleData(en);
addLocaleData(es);
addLocaleData(fr);
addLocaleData(nl);

mixpanel.init(process.env.REACT_APP_MIXPANEL_TOKEN);

const composeEnhancers = process.env.NODE_ENV === 'development'
  ? composeWithDevTools
  : compose || null;

/**
 * Redux Saga
 */
const sagaMiddleware = createSagaMiddleware();

/**
 * Store with reducers and Redux Saga
 */
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);


/**
 * Redux Saga watchers
 */
sagaMiddleware.run(watchUser);
sagaMiddleware.run(watchDocument);
sagaMiddleware.run(watchLocation);
sagaMiddleware.run(watchSecurity);
sagaMiddleware.run(watchAddress);
sagaMiddleware.run(watchProfile);
sagaMiddleware.run(watchAds);

const selectedLang = localStorage.getItem("lang");

if (selectedLang) {
  store.dispatch(localeSet(selectedLang));
} else {

  const language = (navigator.languages && navigator.languages[0]) ||
                       navigator.language ||
                       navigator.userLanguage;

  const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];

  store.dispatch(localeSet(languageWithoutRegionCode));
}

ReactDOM.render((
    <BrowserRouter>
        <Provider store={store}>
          <MixpanelProvider mixpanel={mixpanel}>
            <Route component={App} />
          </MixpanelProvider>
        </Provider>
    </BrowserRouter>
  ), document.getElementById('root'));
registerServiceWorker();
