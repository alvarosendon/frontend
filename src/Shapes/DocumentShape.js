import PropTypes from 'prop-types';
import { documentActionShape } from './DocumentActionShape';

export const documentShape = PropTypes.shape({
  id: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  uploadDate: PropTypes.string.isRequired,
  numberOfPages: PropTypes.number.isRequired,
  numberOfFullPageAds: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  actions: PropTypes.arrayOf(documentActionShape),
});
