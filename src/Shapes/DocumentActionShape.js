import PropTypes from 'prop-types';

export const documentActionShape = PropTypes.shape({
   action: PropTypes.string.isRequired,
   created: PropTypes.string.isRequired
});
