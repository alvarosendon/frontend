
import localeDataEn from './en.json';
import localeDataEs from './es.json';
import localeDataFr from './fr.json';
import localeDataNl from './nl.json';

export default {
  "en": localeDataEn,
  "es": localeDataEs,
  "fr": localeDataFr,
  "nl": localeDataNl
}
