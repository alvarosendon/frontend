import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

class About extends React.PureComponent {

  render () {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
        <ModalHeader toggle={this.props.toggle} tag="h4">
          <FormattedMessage id="about.title" />
        </ModalHeader>
        <ModalBody>
          <div className="row">
            <div className="col">
              <p>
                <FormattedHTMLMessage id="about.content" values={{email: this.props.contactEmail}} />
              </p>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

About.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  className: PropTypes.string,
  contactEmail: PropTypes.string.isRequired
};

export default About;
