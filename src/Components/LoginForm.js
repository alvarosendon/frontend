import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, intlShape } from 'react-intl';
import { required, isEmail, defaultMaxLength } from '../shared/validations';
import Input from '../hoc/Input';
import SimpleInput from '../hoc/SimpleInput';

const LoginForm = (props) => {

  const { handleSubmit, submitting, intl, firstInputRef, handleForgotPassword } = props;

  return (
    <form onSubmit={handleSubmit} >
      <button type="submit" style={{display: "none"}}></button>
      <Field id="txtLoginEmail" name="email" type="email" className="form-control"
          component={Input}
          validate={[ required, defaultMaxLength, isEmail ]}
          label={intl.formatMessage({id: 'login.email.label'})}
          placeholder={intl.formatMessage({id: 'login.email.placeholder'})}
          inputRef={firstInputRef}
        />

      <div className="form-group">
        <label htmlFor="txtLoginPassword" className="font-weight-bold">
          <FormattedMessage id="login.password.label"/>
        </label>
        <a href="#ForgotPassword" className="float-right"
          onClick={handleForgotPassword}>
          <FormattedMessage id="login.forgotPassword"/>
        </a>

        <Field id="txtLoginPassword" name="password" type="password" className="form-control"
            component={SimpleInput}
            validate={[ required, defaultMaxLength ]}
            placeholder={intl.formatMessage({id: 'login.password.placeholder'})}
          />
      </div>

    </form>
  );
}

LoginForm.propTypes = {
    intl: intlShape.isRequired,
    firstInputRef: PropTypes.func.isRequired,
    handleForgotPassword: PropTypes.func.isRequired
};

let InitializeFromStateForm = reduxForm({
  form: 'loginForm',
  touchOnBlur: false
})(LoginForm);

export default InitializeFromStateForm;
