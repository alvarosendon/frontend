import React from 'react';

export const Error = ({errorMessage}) => {

  if (errorMessage === undefined || errorMessage === null || errorMessage === false) {
    return null;
  }

  return (
    <div className="alert alert-danger mt-2 mb-1">
      <div className="d-inline-block">
        <img src={require('../images/warning.svg')} alt="" style={{"width": "32px", "height": "32px"}} />
      </div>
      <div className="d-inline-block ml-3">
        <strong>{errorMessage}</strong>
      </div>
    </div>
  );
};
