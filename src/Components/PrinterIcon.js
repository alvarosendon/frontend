import React from 'react';
import PropTypes from 'prop-types';

export const PrinterIcon = (props) => {
  return (
    <img src={require('../images/print.svg')}
      alt={props.alt}
      title={props.title}
      width={props.width}
      height={props.height}
      className={props.className}
    />
  );
};

PrinterIcon.propTypes = {
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  className: PropTypes.string,
  alt: PropTypes.string,
  title: PropTypes.string,
};

PrinterIcon.defaultProps = {
  width: "38px",
  height: "35px"
};
