import React from 'react';
import PropTypes from 'prop-types';

export const LocationIcon = (props) => {
  return (
    <img src={require('../images/map-location.svg')}
      alt={props.alt}
      title={props.title}
      style={{"width": props.width, "height": props.height}}
      className={props.className}/>
  );
};

LocationIcon.propTypes = {
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  className: PropTypes.string,
  alt: PropTypes.string,
  title: PropTypes.string,
};

LocationIcon.defaultProps = {
  width: "32px",
  height: "32px"
};
