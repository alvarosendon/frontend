import React from 'react';
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, intlShape } from 'react-intl';
import TextArea from '../../hoc/TextArea';
import { required, defaultMaxLength, isEmailList } from '../../shared/validations';

const InviteFriendsByEmailForm = (props) => {

  const { handleSubmit, submitting, intl } = props;

  return (
    <form className="row no-gutters" onSubmit={handleSubmit} autoComplete="off">
      <div className="col-12">
        <Field id="txtFriends" name="friends" className="form-control"
            component={TextArea}
            label={intl.formatMessage({id: "inviteFriends.mailLink"})}
            placeholder="example@example.com, example2@example.com"
            validate={[ required, defaultMaxLength, isEmailList ]}
          />
      </div>
      <div className="col-12 mt-2">
        <button className="btn btn-secondary"><FormattedMessage id="inviteFriends.mailLink.button.send" /></button>
      </div>
    </form>
  );

}

InviteFriendsByEmailForm.propTypes = {
    intl: intlShape.isRequired,
};

let InitializeFromStateForm = reduxForm({
  form: 'inviteFriendsByEmailForm',
  touchOnBlur: false
})(InviteFriendsByEmailForm);

export default InitializeFromStateForm;
