import React from 'react';

export const FormErrors = ({formErrors, className}) => {
  const fields = Object.keys(formErrors);
  const hasErrors = fields.some(fieldName => formErrors[fieldName].length > 0);

  if (hasErrors === false) {
    return null;
  }

  return (
    <ul className={"alert alert-danger" + (className!== undefined?` ${className}`:"")}>
      {fields.map((fieldName, i) => {
        if(formErrors[fieldName].length > 0){
          return (
            <li key={i}>{formErrors[fieldName]}</li>
          )
        } else {
          return '';
        }
      })}
    </ul>
  );
};
