import React from 'react';
import { FormErrors } from './FormErrors';

export const Form = ({className, formErrors, children, onSubmit, errorsClassName, noValidate}) => {
  return (
    <form className={className} onSubmit={onSubmit} noValidate={noValidate}>
      <FormErrors formErrors={formErrors} className={errorsClassName}/>
      {children}
    </form>
  );
};
