import React from 'react';
import PropTypes from 'prop-types';
import "./Loader.css";

const Loader = ({message, className}) => {

  let loaderText = message;
  if (message === undefined || message === null || message === "") {
    loaderText = "";
  }

  return (
    <div className={"loader-container " + className}>
      <div className="loader">
        {loaderText}
      </div>
    </div>
  );

};

Loader.propTypes = {
  message: PropTypes.string,
}

export default Loader;
