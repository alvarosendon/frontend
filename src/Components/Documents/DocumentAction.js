import React from 'react';
import Moment from 'react-moment';
import { FormattedMessage } from 'react-intl';
import { documentActionShape } from '../../Shapes';

const DocumentAction = ({action}) => {
  return (
    <tr>
      <td className="text-muted">
        <Moment parse="DD/MM/YYYY HH:mm" format="DD/MM/YYYY HH:mm">{action.created}</Moment>
      </td>
      <td>
        <FormattedMessage id={"documents.action." + action.action} />
      </td>
    </tr>
  );

};

DocumentAction.propTypes = {
  action: documentActionShape.isRequired
}

export default DocumentAction;
