import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { FormattedMessage } from 'react-intl';
import { documentShape } from '../../Shapes';
import { calendarStrings, numberOfDaysToDelete } from '../../shared/utils';
import DocumentAction from './DocumentAction';
import Wrapper from '../../hoc/Wrapper';

import './DocumentDetail.css'

const DocumentDetail = ({doc, onClickReportProblem}) => {

  let actions = null;

  if (doc.actions !== undefined && doc.actions !== null) {
    actions = doc.actions.map((a, index) =>
      <DocumentAction key={doc.id + "_action_" + index} action={a} />
    )
  }

  let buttons = null;

  if (doc.status === "CONVERTED") {
    buttons = (
      <div className="pt-2" style={{"borderTop": "2px solid #dee2e6"}}>
        <a href={doc.url} target="_blank" className="btn btn-secondary btn-sm mr-3">
          <FormattedMessage id="documents.item.btn.download" />
        </a>
        <a href="#ReportProblem" className="btn btn-secondary btn-sm" onClick={onClickReportProblem}>
          <FormattedMessage id="documents.item.btn.problem" />
        </a>
      </div>
    );
  }

  return (
    <Wrapper>
      <div>{doc.name}</div>
      <div className="text-muted"><Moment  parse="DD/MM/YYYY HH:mm" calendar={calendarStrings}>{doc.uploadDate}</Moment></div>
      <div className="text-muted">
        <FormattedMessage id="documents.item.pages" values={{numberOfPages: doc.numberOfPages}} />
        (<FormattedMessage id="documents.item.fullPageAds" values={{numberOfAds: doc.numberOfFullPageAds}} />)
      </div>
      <div className="mt-2">
        <table className="table">
          <tbody>
            {actions}
            <tr>
              <td className="text-muted">
                <Moment add={{"days": numberOfDaysToDelete}} parse="DD/MM/YYYY HH:mm" format="DD/MM/YYYY">{doc.uploadDate}</Moment>
              </td>
              <td><FormattedMessage id="documents.item.action.deleted" /></td>
            </tr>
          </tbody>
        </table>
      </div>
      {buttons}
    </Wrapper>
  );

};

DocumentDetail.propTypes = {
  doc: documentShape.isRequired,
  onClickReportProblem: PropTypes.func.isRequired
}

export default DocumentDetail;
