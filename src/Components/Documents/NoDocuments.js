import React, {Component} from 'react';
import { connect } from "react-redux";
import { FormattedMessage, FormattedHTMLMessage, intlShape } from 'react-intl';
import { LocationIcon, PrinterIcon } from '../Icons';

import './NoDocuments.css'

class NoDocuments extends Component {

  renderItem(icon, primaryTextKey, secondaryTextKey, showArrow) {

    const arrow = (
      <div className="d-inline-block">
        <span className="d-none d-lg-inline">
          <i className="arrow right"></i>
        </span>
      </div>
    );

    return (
      <div className="ml-4 step">
        <div className="d-inline-block">
          {icon}
        </div>
        <div className="d-inline-block ml-2 text-left">
          <strong>
            <FormattedMessage id={primaryTextKey} />
          </strong>
          <br />
          <small className="text-secondary">
            <FormattedMessage id={secondaryTextKey} />
          </small>
        </div>
        {showArrow===true?arrow:null}
      </div>
    );


  }

  render() {

      const newDocumentIcon = (
        <span className="fa-stack align-baseline icon">
          <i className="fas fa-file fa-stack-1x"></i>
          <i className="fas fa-plus fa-stack-1x fa-inverse" style={{"paddingTop": "15px"}}></i>
        </span>
      );

      const locationIcon = (
          <LocationIcon className="align-baseline " width="27px" height="44px" />
      );

      const printIcon = (
        <PrinterIcon className="align-baseline icon" />
      );

      return (
        <div className="noDocuments">
          <div className="title pt-3 pl-3 pr-3 pb-3">
            <h1 className="text-center">
              <FormattedHTMLMessage id="documents.noDocs.title" />
            </h1>
            <div className="text-center">
              <FormattedMessage id="documents.noDocs.subtitle" />
            </div>
          </div>
          <div className="subtitle pt-4 pb-3">
            <h4 className="text-center">
              <FormattedMessage id="documents.noDocs.howItWorks.title" />
            </h4>

            <div className="mx-md-auto mr-sm-2 ml-sm-2 text-center" style={{"maxWidth": "726px"}}>
              <div>
                <FormattedMessage id="documents.noDocs.howItWorks.description" />
              </div>
              <div>
                <strong className="underline">
                  <FormattedMessage id="documents.noDocs.howItWorks.forFree" />
                </strong>
              </div>
            </div>

            <div className="mx-auto mt-3 row ">

              <div className="col-sm-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3 steps pb-2 pt-3 d-lg-flex justify-content-center">
                  {this.renderItem(newDocumentIcon, "documents.noDocs.howItWorks.upload", "documents.noDocs.howItWorks.upload.sub", true)}
                  {this.renderItem(locationIcon, "documents.noDocs.howItWorks.locate", "documents.noDocs.howItWorks.locate.sub", true)}
                  {this.renderItem(printIcon, "documents.noDocs.howItWorks.print", "documents.noDocs.howItWorks.print.sub", false)}
              </div>
            </div>

          </div>
        </div>
      );
  }
}

NoDocuments.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
  };
}

const mapDispatchToProps = {  }

export default connect(mapStateToProps, mapDispatchToProps)(NoDocuments);
