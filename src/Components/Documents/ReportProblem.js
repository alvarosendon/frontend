import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

const ReportProblem = ({isOpen, toggle, onSubmit, problem, onChangeProblem, intl}) => {

  let textAreaRef;

  return (
    <Modal isOpen={isOpen} toggle={toggle} onOpened={() => textAreaRef.focus()}>
      <ModalHeader toggle={toggle}>
        <FormattedMessage id="document.reportProblem.title" />
      </ModalHeader>
      <ModalBody>
        <div className="form-group">
          <label htmlFor="txtDocumentProblem">
            <FormattedMessage id="document.reportProblem.description" />
          </label>
          <textarea ref={(node) => {textAreaRef = node;}} className="form-control" id="txtDocumentProblem" name="problem"
            placeholder={intl.formatMessage({id: "document.reportProblem.description.placeholder"})}
            value={problem}
            onChange={onChangeProblem}
            />
        </div>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={onSubmit}>
          <FormattedMessage id="document.reportProblem.btn.send" />
        </Button>
      </ModalFooter>
    </Modal>
  );

};

ReportProblem.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  problem: PropTypes.string.isRequired,
  onChangeProblem: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  intl: intlShape.isRequired
}

export default ReportProblem;
