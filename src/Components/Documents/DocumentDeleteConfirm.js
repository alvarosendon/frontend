import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

const DocumentDeleteConfirm = ({isOpen, toggle, onDelete}) => {

  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>
        <FormattedMessage id="confirm.title" />
      </ModalHeader>
      <ModalBody>
        <FormattedMessage id="documents.delete.confirm" />
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={onDelete}>
          <FormattedMessage id="confirm.btn.ok" />
        </Button>
        &nbsp;
        <Button color="secondary" onClick={toggle}>
          <FormattedMessage id="confirm.btn.cancel" />
        </Button>
      </ModalFooter>
    </Modal>
  );

};

DocumentDeleteConfirm.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
}

export default DocumentDeleteConfirm;
