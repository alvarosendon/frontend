import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDropzone from 'react-dropzone';
import { FormattedMessage, intlShape } from 'react-intl';
import Loader from '../Loader';

import Wrapper from '../../hoc/Wrapper';

import './DocumentUpload.css';
import image from '../../images/documents.svg';

import { uploadDocument, toggleLogIn } from '../../store/actions';

class DocumentUpload extends Component {

  constructor(props) {
    super(props);

    this.handleUploadButtonClick = this.handleUploadButtonClick.bind(this);
  }

  handleUploadButtonClick(event, dropRef) {
    event.preventDefault();

    if (this.props.loggedIn === false) {
      this.props.toggleLogIn();
    }
  }

  onDrop(files) {

      if (this.props.loggedIn === false) {
        this.props.toggleLogIn();
        return;
      }

      // mixpanel.track('Upload documents click');

      files.forEach(file => {
          this.props.uploadDocument(file);
      });
  }

  render () {

    let dropRef;

    const dropZone =
        this.props.uploading
        ? <div className="Dropzone">
            <div className="text-center">
              <Loader className="mt-4 mb-2" message={this.context.intl.formatMessage({id: "documents.upload.text"})} />
              <br />
              <button id="btnUploadDocuments" className="btn btn-accent btn-lg" disabled>
                  <FormattedMessage id="documents.upload.text" />
              </button>
            </div>
          </div>
        : <ReactDropzone
            className="Dropzone"
            disableClick={!this.props.loggedIn}
            ref={(node) => {dropRef = node;}}
            onDrop={this.onDrop.bind(this)}
            multiple
            accept="application/pdf">
            <div className="text-center mt-5">
              <img src={image} alt={this.context.intl.formatMessage({id: 'documents.upload.button'})} />
              <br />
              <button id="btnUploadDocuments" className="btn btn-accent btn-lg" onClick={(e) => this.handleUploadButtonClick(e, dropRef)}>
                  <FormattedMessage id="documents.upload.button" />
              </button>
            </div>
        </ReactDropzone>;
    return (
        <Wrapper>
            {dropZone}
            <div>
              <span className="text-secondary">
                <FormattedMessage id="documents.upload.limit" values={{amount: 20 }} />
              </span>&nbsp;
              <a target="_blank" href="http://bfy.tw/3Lc6" rel="noopener noreferrer">
                <FormattedMessage id="documents.upload.makePdf" />
              </a>
            </div>
        </Wrapper>
    );
  }
}

DocumentUpload.contextTypes = {
    intl: intlShape.isRequired,
    mixpanel: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        files: state.documents.files,
        uploading: state.documents.uploading,
        loggedIn: state.security.loggedIn
    };
};

const mapDispatchToProps = {
    uploadDocument, toggleLogIn
};

export default connect(mapStateToProps, mapDispatchToProps)(DocumentUpload);
