import React from 'react';
import PropTypes from 'prop-types';
import { Popover, PopoverBody } from 'reactstrap';
import Moment from 'react-moment';
import { FormattedMessage, intlShape } from 'react-intl';
import { calendarStrings } from '../../shared/utils';
import { documentShape } from '../../Shapes';
import DocumentDetail from './DocumentDetail';
import DocumentStatus from './DocumentStatus';
import infoImage from '../../images/info.svg';

class Document extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      popoverOpen: false
    }

    this.togglePopover = this.togglePopover.bind(this);
  }

  togglePopover(e) {
    if (e !== undefined) {
        e.preventDefault();
    }

    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  render() {

    const { doc, onSelect, onClickReportProblem } = this.props;

    return (
      <tr>
        <td>
          <input type="checkbox" onClick={onSelect} disabled={doc.status !== "CONVERTED"}/>
        </td>
        <td>
          {doc.name}
          <div>
            <span className="text-muted small">
              <Moment parse="DD/MM/YYYY HH:mm" calendar={calendarStrings}>{doc.uploadDate}</Moment>
              {" - "}
              <FormattedMessage id="documents.item.pages" values={{numberOfPages: doc.numberOfPages}} />
            </span>
          </div>
        </td>
        <td>
          <DocumentStatus status={doc.status} />
        </td>
        <td>
          <a href="#Detail" id={"popover" + doc.id} onClick={this.togglePopover}
            title={this.context.intl.formatMessage({id: 'documents.item.btn.details'})}>
            <img src={infoImage} alt={this.context.intl.formatMessage({id: 'documents.item.btn.details'})} />
          </a>
          <Popover className="document-detail" placement="left" isOpen={this.state.popoverOpen} target={"popover" + doc.id} toggle={this.togglePopover}>
            <PopoverBody>
              <DocumentDetail doc={doc} onClickReportProblem={(e) => {this.togglePopover();onClickReportProblem(e);}}/>
            </PopoverBody>
          </Popover>

        </td>
      </tr>
    );
  }
}

Document.propTypes = {
  doc: documentShape.isRequired,
  onSelect: PropTypes.func.isRequired,
  onClickReportProblem: PropTypes.func.isRequired,
}

Document.contextTypes = {
    intl: intlShape.isRequired,
};

export default Document;
