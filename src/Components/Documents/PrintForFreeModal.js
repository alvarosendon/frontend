import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { PrinterIcon } from '../Icons';

import './PrintForFreeModal.css'

class PrintForFreeModal extends React.PureComponent {

  render () {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
        <ModalHeader toggle={this.props.toggle} tag="h4">
          <FormattedMessage id="documents.printForFree.title" />
        </ModalHeader>
        <ModalBody>
          <div className="row">
            <div className="col-md-4">
              <div className="text-center" style={{"backgroundColor": "#edf6e2", "padding": "20px", "borderRadius": "10px"}}>
                <PrinterIcon className="align-baseline" width="70px" height="64px" />
              </div>
            </div>
            <div className="col-md-8">
              <span style={{fontSize: "24px"}}>
                <FormattedMessage id="documents.printForFree.subtitle" />
              </span>
              <ol className="mt-3 printForFree">
                <li><FormattedMessage id="documents.printForFree.line1" /></li>
                <li><FormattedMessage id="documents.printForFree.line2" values={{email: this.props.email}}/></li>
                <li><FormattedMessage id="documents.printForFree.line3" /></li>
              </ol>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

PrintForFreeModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired
};

export default PrintForFreeModal;
