import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const DocumentStatus = ({status}) => {

  let type = "";
  switch (status) {
    case "IN_PROCESS":
      type = "in-progress";
      break;
    case "CONVERTED":
      type = "ready";
      break;
    case "ERROR":
      type = "error";
      break;
    default:
      console.log("Invalid status: {}", status);
      break;
  }

  return (
    <div className="">
      <span className={"mr-2 circle circle-" + type}></span>
      <FormattedMessage id={"documents.state." + status} />
    </div>
  );

};

DocumentStatus.propTypes = {
  status: PropTypes.oneOf(['IN_PROCESS', 'CONVERTED', 'ERROR']),
}

export default DocumentStatus;
