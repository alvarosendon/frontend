import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, intlShape } from 'react-intl';
import { required, defaultMaxLength } from '../../shared/validations';
import SimpleInput from '../../hoc/SimpleInput';

const PromoCodeForm = (props) => {

  const { handleSubmit, submitting, intl } = props;

  return (
    <form className="form-inline" onSubmit={handleSubmit}>
      <button type="submit" style={{display: "none"}}></button>
      <div>
        <Field name="promoCode" type="text" className="form-control form-control-sm mr-sm-2"
            component={SimpleInput}
            validate={[ required, defaultMaxLength ]}
            placeholder={intl.formatMessage({id: 'earnCredits.promo.description'})}
          />
      </div>
      <button className="btn btn-secondary btn-sm mb-2">
        <FormattedMessage id="earnCredits.promo.button" />
      </button>
    </form>
  );
}

PromoCodeForm.propTypes = {
    intl: intlShape.isRequired,
};

let InitializeFromStateForm = reduxForm({
  form: 'promoCodeForm',
  touchOnBlur: false
})(PromoCodeForm);

export default InitializeFromStateForm;
