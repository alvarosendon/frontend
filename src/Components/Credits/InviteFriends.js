import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import InviteFriendByEmailForm from '../Form/InviteFriendsByEmailForm';
import { FacebookShareButton } from 'react-share';

import './InviteFriends.css'

class InviteFriends extends Component {

  getLink() {
    return this.props.user.link !== undefined? this.props.user.link: "";
  }

  render() {

    const shareLink = this.getLink();

    return (
      <div className="container-fluid">
        <div><FormattedMessage id="inviteFriends.title" /></div>

        <FacebookShareButton
            url={shareLink}
            className="btn btn-accent btn-block mt-4">
            <FormattedMessage id="inviteFriends.shareOnFacebook" />
        </FacebookShareButton>

        <div className="mt-5">
          <div><strong><FormattedMessage id="inviteFriends.shareLink" /></strong></div>
          <div className="row no-gutters">
              <div className="col-9">
                <input type="text" value={shareLink} className="form-control" disabled />
              </div>
              <div className="col-2 ml-4">
                <button className="btn btn-secondary" onClick={this.props.onCopyLink}>
                  <i className="far fa-copy"></i>
                  &nbsp;<FormattedMessage id="inviteFriends.shareLink.button.copy" />
                </button>
              </div>
          </div>
        </div>

        <div className="mt-5">
          <InviteFriendByEmailForm
            intl={this.context.intl}
            onSubmit={this.props.onInviteFriends} />
        </div>

      </div>
    );
  }
}

InviteFriends.propTypes = {
  onInviteFriends: PropTypes.func.isRequired,
  onCopyLink: PropTypes.func.isRequired
}

InviteFriends.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    "user": state.user.user
  };
}

const mapDispatchToProps = { }

export default connect(mapStateToProps, mapDispatchToProps)(InviteFriends);
