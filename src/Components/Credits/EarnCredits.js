import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import SectionList from '../../Profile/SectionList'
import { toggleProfile, fetchSections, userFillPromoCode, userInviteFriends, userCopyLink } from '../../store/actions';
import InviteFriends from './InviteFriends';
import PromoCodeForm from './PromoCodeForm';

import './EarnCredits.css'

class EarnCredits extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpenInviteFriends: false
    }

    this.handleProfileToggle = this.handleProfileToggle.bind(this);
    this.handleCompleteProfileClick = this.handleCompleteProfileClick.bind(this);
    this.handlePromoSubmit = this.handlePromoSubmit.bind(this);
    this.toggleInviteFriendsModal = this.toggleInviteFriendsModal.bind(this);
    this.handleInviteFriendsSubmit = this.handleInviteFriendsSubmit.bind(this);
    this.handleCopyLink = this.handleCopyLink.bind(this);
  }

  componentWillMount() {

    if (this.props.loggedIn === true) {
        this.props.fetchSections();
    }

  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.loggedIn === false && this.props.loggedIn === true) {
      this.props.fetchSections();
    }
  }

  handleCompleteProfileClick(e) {
    e.preventDefault();

    this.handleProfileToggle();
  }

  handleProfileToggle(e) {
    if (e !== undefined) {
      e.preventDefault();
    }

    this.props.toggleProfile();
  }

  handlePromoSubmit(model) {

    this.props.userFillPromoCode(model.promoCode, this.context.intl);

  }

  handleInviteFriendsSubmit(model) {

    this.props.userInviteFriends(model.friends, this.context.intl);

  }

  handleCopyLink() {

    this.props.userCopyLink(this.props.user.link !== undefined? this.props.user.link: "", this.context.intl);

  }

  toggleInviteFriendsModal(e) {

    if (e !== undefined) {
      e.preventDefault();
    }

    this.setState({isOpenInviteFriends: !this.state.isOpenInviteFriends});
  }

  renderCurrentBalance(credits) {

    const limitCreditsForWarning = 25;

    const warningIcon = (
      <img src={require('../../images/warning.svg')} alt="" style={{"width": "32px", "height": "32px"}} />
    );

    const warning = credits <= limitCreditsForWarning? warningIcon: null;

    const message = this.context.intl.formatMessage({id: 'earnCredits.balance'}, {balance: credits});

    return (
      <p>
        {warning}
        <span className={credits <= limitCreditsForWarning? "text-danger": ""}>
          {message}
        </span>
      </p>
    );

  }

  render () {

    const { loggedIn, user, sections } = this.props;

    if (loggedIn === false || user === undefined) {
      return null;
    }

    let credits = 0;

    if (user.printCredits !== undefined) {
      credits = user.printCredits;
    }

    let completeProfile = null;

    if (sections !== undefined && sections.length !== 0) {
      completeProfile = (
        <div className="mt-3">
          <h5><FormattedMessage id="earnCredits.profile.title" /></h5>
          <p><FormattedMessage id="earnCredits.profile.description" /></p>
          <a className="btn btn-secondary btn-sm mt-2" role="button" href="#CompleteProfile"
            onClick={this.handleCompleteProfileClick}>
            <FormattedMessage id="earnCredits.profile.button" />
          </a>

          <SectionList toggle={this.handleProfileToggle} />
        </div>
      );
    }

    return (
      <div className="earnCredits">
        <div className="card zc-card">
          <div className="card-body">
            <div className="">
              <h4><FormattedMessage id="earnCredits.title" /></h4>
              {this.renderCurrentBalance(credits)}
              <p className="mt-2"><FormattedMessage id="earnCredits.description" /></p>
            </div>

            {completeProfile}

            <div className="mt-3">
              <h5><FormattedMessage id="earnCredits.friends.title" /></h5>
              <p><FormattedMessage id="earnCredits.friends.description" /></p>
              <a className="btn btn-secondary btn-sm mt-2" role="button" href="#InviteFriends"
                onClick={this.toggleInviteFriendsModal}>
                <FormattedMessage id="earnCredits.friends.button" />
              </a>

              <Modal isOpen={this.state.isOpenInviteFriends} toggle={this.toggleInviteFriendsModal}>
                <ModalHeader toggle={this.toggleInviteFriendsModal} tag="h4">
                  <FormattedMessage id="earnCredits.friends.title" />
                </ModalHeader>
                <ModalBody>
                  <InviteFriends
                    onInviteFriends={this.handleInviteFriendsSubmit}
                    onCopyLink={this.handleCopyLink}/>
                </ModalBody>
              </Modal>
            </div>

            <div className="mt-3">
              <h5><FormattedMessage id="earnCredits.promo.title" /></h5>
              <div className="">
                <PromoCodeForm intl={this.context.intl} onSubmit={this.handlePromoSubmit}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

EarnCredits.propTypes = {
};

EarnCredits.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    user: state.user.user,
    loggedIn: state.security.loggedIn,
    sections: state.profile.list
  };
}

const mapDispatchToProps = { toggleProfile, fetchSections, userFillPromoCode, userInviteFriends, userCopyLink }

export default connect(mapStateToProps, mapDispatchToProps)(EarnCredits);
