import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { intlShape } from 'react-intl';
import { required, isEmail, defaultMaxLength } from '../shared/validations';
import Input from '../hoc/Input';

const ForgotPasswordForm = (props) => {

  const { handleSubmit, submitting, intl, firstInputRef } = props;

  return (
    <form onSubmit={handleSubmit} >
      <button type="submit" style={{display: "none"}}></button>

      <Field id="txtForgotPasswordEmail" name="email" type="email" className="form-control"
          component={Input}
          validate={[ required, defaultMaxLength, isEmail ]}
          label={intl.formatMessage({id: 'forgotPassword.field.email'})}
          placeholder={intl.formatMessage({id: 'forgotPassword.field.email.placeholder'})}
          inputRef={firstInputRef}
        />

    </form>
  );
}

ForgotPasswordForm.propTypes = {
    intl: intlShape.isRequired,
    firstInputRef: PropTypes.func.isRequired
};

let InitializeFromStateForm = reduxForm({
  form: 'forgotPasswordForm',
  touchOnBlur: false
})(ForgotPasswordForm);

export default InitializeFromStateForm;
