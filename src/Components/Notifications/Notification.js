import React from 'react';
import PropTypes from 'prop-types';

export default class Notification extends React.Component {

  renderIcon() {
    if (this.props.type === "error") {
      return (
        <i className="fas fa-exclamation-triangle text-danger"></i>
      );
    } else {
      return (
        <i className="fas fa-check-circle text-success"></i>
      );
    }
  }

  render () {
    return (
          <div className="dropdown-item notification-item">
            <div className="row">
              <div className="col-1">
                { this.renderIcon() }
              </div>
              <div className="col-11">
                <span className="notification-text">{this.props.text}</span>
              </div>
            </div>
          </div>
    );
  }
}

Notification.propTypes = {
  type: PropTypes.oneOf(['error', 'success']),
  text: PropTypes.string.isRequired
};
