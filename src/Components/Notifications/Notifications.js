import React from 'react';
import { connect } from "react-redux";

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

import Notification from "./Notification";

import './Notifications.css';

class Notifications extends React.Component {

  constructor(props) {
    super(props);

    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.handleMarkAllAsRead = this.handleMarkAllAsRead.bind(this);

    this.state = {
      isOpen: false,
      dropdownOpen: false
    };
  }

  componentWillMount() {

  }

  toggleDropdown() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  handleMarkAllAsRead(e) {

    e.preventDefault();
  }

  render () {
    return (
      <Dropdown nav className="mr-2" isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
        <DropdownToggle nav>
          <i className="fas fa-bell"></i>&nbsp;
          <span className="badge badge-danger">2</span>
        </DropdownToggle>
        <DropdownMenu right className="notifications">
          <div className="notifications-header">
            <div>
              Notifications (2)
              <a href="#MarkAllNotificationsAsRead" className="float-right"
                onClick={this.handleMarkAllAsRead}>Mark all as read</a>
            </div>
          </div>
          <DropdownItem divider />

          <Notification type="success" text="Document is ready to be printed."/>
          <Notification type="error" text="We cannot send the requested delivery to you because the document was not printed."/>

        </DropdownMenu>
      </Dropdown>
    );
  }
}

Notifications.propTypes = {
};

function mapStateToProps(state) {
  return {

  };
}

const mapDispatchToProps = {  }

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
