import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import { Link } from 'react-router-dom';
import Wrapper from '../hoc/Wrapper';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

import { logout, logIn, toggleLogIn, fetchUser,
  userForgotPasswordToggle, userCreateAccountToggle } from '../store/actions';
import Notifications from './Notifications/Notifications';
import CreateAccountModal from '../Security/CreateAccountModal';
import LoginModal from '../Security/LoginModal';
import ForgotPasswordModal from '../Security/ForgotPasswordModal';

import './Toolbar.css';

class Toolbar extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.handleOpenLogIn = this.handleOpenLogIn.bind(this);
    this.handleOnLogIn = this.handleOnLogIn.bind(this);
    this.handleOpenCreateAccount = this.handleOpenCreateAccount.bind(this);

    this.state = {
      isOpen: false,
      dropdownOpen: false,
    };
  }

  componentWillMount() {

    if (this.props.user === undefined && this.props.loggedIn === true) {
      this.props.fetchUser();
    }
  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.loggedIn === false && this.props.loggedIn === true) {
      this.props.fetchUser();
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleDropdown() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  handleOpenLogIn(e) {
    e.preventDefault();

    this.props.toggleLogIn();
  }

  handleOnLogIn(username, password, rememberMe) {

    this.props.logIn(username, password);
  }

  handleOpenCreateAccount(e) {
    e.preventDefault();

    this.props.userCreateAccountToggle();
  }

  renderForUser() {
    return (
      <Nav className="ml-auto" navbar>
        <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown} className="mr-3">
          <DropdownToggle nav caret id="toolbarLinkUser">
            {this.props.user !== undefined? this.props.user.fullName : "" }
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem tag={Link} to="/user/account" id="toolbarLinkUserAccount">
              <img src={require('../images/account.svg')} alt=""
                style={{"width": "32px", "height": "32px"}} />
              &nbsp;
              <FormattedMessage id="toolbar.logged.account" /></DropdownItem>
            <DropdownItem onClick={this.props.logout} id="toolbarLinkLogout">
              <img src={require('../images/log-out.svg')} alt=""
                style={{"width": "32px", "height": "32px"}} />
              &nbsp;
              <FormattedMessage id="toolbar.logged.logout" /></DropdownItem>
          </DropdownMenu>
        </Dropdown>
        <Notifications />
      </Nav>
    );
  }

  renderForAnonymous() {
    return (
      <Nav className="ml-auto mr-3" navbar>
        <ul className="navbar-nav mr-auto">
          <NavItem className="mr-3">
            <a id="toolbarLinkLogIn" className="nav-link" href="#LogIn" onClick={this.handleOpenLogIn}>
              <FormattedMessage id="toolbar.logIn" />
            </a>

            <LoginModal
              isOpen={this.props.logInModal.isOpen}
              toggle={this.props.toggleLogIn}
              onLogIn={this.handleOnLogIn}
              onCreateAccount={this.props.userCreateAccountToggle}
              onForgotPassword={this.props.userForgotPasswordToggle} />

            <ForgotPasswordModal
              isOpen={this.props.openForgotPassword}
              toggle={this.props.userForgotPasswordToggle} />

          </NavItem>
          <NavItem className="accent">
            <a id="toolbarLinkCreateAccount" className="nav-link" href="#CreateAccount" onClick={this.handleOpenCreateAccount}>
              <FormattedMessage id="toolbar.createAccount" />
            </a>

            <CreateAccountModal isOpen={this.props.openCreateAccount}
              toggle={this.props.userCreateAccountToggle} />
          </NavItem>
        </ul>
      </Nav>
    );
  }

  render () {

    let content;

    if (this.props.loggedIn === false || this.props.user === undefined) {
      content = this.renderForAnonymous();
    } else {
      content = this.renderForUser();
    }

    return (
      <Wrapper>

        <Navbar color="zerocopy" light expand="md">
          <NavbarBrand tag={Link} to="/" className="ml-3">
            <img src={require("../images/logo-white-h.png")} alt="Zerocopy" width="178" height="30" />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle}>
            <i className="fas fa-bars"></i>
          </NavbarToggler>

          <Collapse isOpen={this.state.isOpen} navbar>
              {content}
          </Collapse>
        </Navbar>

      </Wrapper>
    );
  }

}

Toolbar.propTypes = {
};

Toolbar.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    loggedIn: state.security.loggedIn,
    user: state.user.user,
    logInModal: state.security.logIn,
    openCreateAccount: state.user.createAccount.open,
    openForgotPassword: state.user.forgotPassword.open
  };
}

const mapDispatchToProps = {
    fetchUser, logout, logIn, toggleLogIn, userForgotPasswordToggle, userCreateAccountToggle
};

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);
