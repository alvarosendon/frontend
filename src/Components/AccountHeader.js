import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

import './AccountHeader.css'

export default class AccountHeader extends React.Component {

  render () {
    /**
    <Link className={this.props.activeIndex === 3? "btn btn-secondary btn-sm mr-3": "btn btn-sm mr-3"}
      role="button" to="/user/stats">
      Stats
    </Link>
    */
    return (
      <div id="accountHeader" style={{borderBottom: "3px solid #e4e8ec"}} className="pb-3">
        <Link id="accountGeneral" className={this.props.activeIndex === 1? "btn btn-secondary btn-sm mr-3": "btn btn-sm mr-3"}
          role="button" to="/user/account">
          <FormattedMessage id="account.general.title" />
        </Link>
        <Link id="accountAddresses" className={this.props.activeIndex === 2? "btn btn-secondary btn-sm mr-3": "btn btn-sm mr-3"}
          role="button" to="/user/addresses">
          <FormattedMessage id="address.list.title" />
        </Link>

        <Link id="accountBack" className="btn btn-sm mr-3 float-right" role="button" to="/">
          <FormattedMessage id="btn.back" />
        </Link>
      </div>
    );
  }

}

AccountHeader.propTypes = {
  activeIndex: PropTypes.number.isRequired
};
