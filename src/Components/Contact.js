import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

class Contact extends React.PureComponent {

  render () {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
        <ModalHeader toggle={this.props.toggle} tag="h4">
          <FormattedMessage id="contact.title" />
        </ModalHeader>
        <ModalBody>
          <div className="row">
            <div className="col">
              <p>
                <FormattedMessage id="contact.brand" />
              </p>
              <FormattedHTMLMessage id="contact.detail" values={{email: this.props.contactEmail}}/>
              <div>
                <a href={"mailto:" + this.props.contactEmail}><i class="fas fa-envelope"></i> {this.props.contactEmail}</a>
              </div>
              <div>
                <a href="tel:+32 800 820 19"><i class="fas fa-phone"></i> +32 800 820 19</a>
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

Contact.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  className: PropTypes.string,
  contactEmail: PropTypes.string.isRequired
};

export default Contact;
