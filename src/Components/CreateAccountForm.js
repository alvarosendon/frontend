import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, FormattedHTMLMessage, intlShape } from 'react-intl';
import { required, isEmail, defaultMaxLength } from '../shared/validations';
import Input from '../hoc/Input';
import SimpleInput from '../hoc/SimpleInput';

const CreateAccountForm = (props) => {

  const { handleSubmit, submitting, intl, firstInputRef } = props;

  return (
    <form onSubmit={handleSubmit} >
      <button type="submit" style={{display: "none"}}></button>
      
      <Field id="txtCreateAccountEmail" name="email" type="email" className="form-control"
          component={Input}
          validate={[ required, defaultMaxLength, isEmail ]}
          label={intl.formatMessage({id: 'createAccount.field.email'})}
          placeholder={intl.formatMessage({id: 'createAccount.field.email.placeholder'})}
          inputRef={firstInputRef}
        />

        <div className="form-group">
          <label htmlFor="txtCreateAccountPassword" className="font-weight-bold">
            <FormattedMessage id="createAccount.field.password" />
          </label>

          <Field id="txtCreateAccountPassword" name="password" type="password" className="form-control"
              component={SimpleInput}
              validate={[ required, defaultMaxLength ]}
              placeholder={intl.formatMessage({id: 'createAccount.field.password.placeholder'})}
            />
          <Field name="passwordRepeat" type="password" className="form-control mt-2"
              component={SimpleInput}
              validate={[ required, defaultMaxLength ]}
              placeholder={intl.formatMessage({id: 'createAccount.field.repeatPassword.placeholder'})}
            />
        </div>

        <div className="form-group">
          <div className="form-check">
            <Field id="chkCreateAccountPolicy" name="policy" type="checkbox" className="form-check-input"
                component={SimpleInput}
                validate={[ required ]}
                placeholder={intl.formatMessage({id: 'createAccount.field.password.placeholder'})}
              />
            <label className="form-check-label" htmlFor="chkCreateAccountPolicy">
              <FormattedHTMLMessage id="createAccount.field.policy" />
            </label>
          </div>
        </div>

    </form>
  );
}

CreateAccountForm.propTypes = {
    intl: intlShape.isRequired,
    firstInputRef: PropTypes.func.isRequired
};

let InitializeFromStateForm = reduxForm({
  form: 'createAccountForm',
  touchOnBlur: false
})(CreateAccountForm);

export default InitializeFromStateForm;
