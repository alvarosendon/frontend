import React from 'react';
import PropTypes from 'prop-types';

export const NoMatch = (props) => {

  let className = "container-fluid text-center";

  if (props.loggedIn === false) {
    className += " mt-4";
  }

  return (
    <div className={className}>
      <h2 className="text-danger">
        <i className="fas fa-exclamation-circle"></i>&nbsp;
        {props.text}
      </h2>
    </div>
  );
};

NoMatch.propTypes = {
  text: PropTypes.string,
};

NoMatch.defaultProps = {
  text: "Ups we don't found this page.",
};
