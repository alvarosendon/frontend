import React from 'react';
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, intlShape } from 'react-intl';
import { required, requiredIf, isEmail, defaultMaxLength } from '../shared/validations';
import Input from '../hoc/Input';


const requiredPassword = requiredIf("passwordRepeat");
const requiredPasswordRepeat = requiredIf("password");

const AccountForm = (props) => {

  const { handleSubmit, submitting, intl } = props;

  return (
    <form onSubmit={handleSubmit} autoComplete="off">

      <div className="row mt-3">
        <div className="col-md-5">
          <Field id="txtAccountFullName" name="fullName" type="text" className="form-control"
              component={Input}
              label={intl.formatMessage({id: "account.general.field.fullName"})}
              validate={[ required, defaultMaxLength ]}
            />
          </div>
        <div className="col-md-5">
          <Field id="txtAccountUsername" name="username" type="text" className="form-control"
              component={Input}
              label={intl.formatMessage({id: "account.general.field.username"})}
              validate={[ required, defaultMaxLength ]}
            />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-md-5">
          <Field id="txtAccountEmail" name="email" type="email" className="form-control" autoComplete="nope"
              component={Input}
              label={intl.formatMessage({id: "account.general.field.email"})}
              validate={[ required, defaultMaxLength, isEmail ]}
            />
        </div>
      </div>

      <div className="row mt-3">
        <div className="col-md-5">
          <Field id="txtAccountPassword" name="password" type="password" className="form-control" autoComplete="new-password"
              component={Input}
              label={intl.formatMessage({id: "account.general.field.password"})}
              placeholder={intl.formatMessage({id: "account.general.field.password.placeholder"})}
              validate={[ defaultMaxLength, requiredPassword ]}
            />
        </div>
        <div className="col-md-5">
          <Field id="txtAccountPasswordRepeat" name="passwordRepeat" type="password" className="form-control" autoComplete="new-password"
              component={Input}
              label={intl.formatMessage({id: "account.general.field.repeatPassword"})}
              placeholder={intl.formatMessage({id: "account.general.field.repeatPassword.placeholder"})}
              validate={[ defaultMaxLength, requiredPasswordRepeat ]}
            />
        </div>
      </div>

      <div className="row">
        <div className="col-md-12">
          <button className="btn btn-primary btn-md">
            <FormattedMessage id="account.general.btn.save" />
          </button>
        </div>
      </div>
    </form>
  );

}

AccountForm.propTypes = {
    intl: intlShape.isRequired,
};

let InitializeFromStateForm = reduxForm({
  form: 'accountForm'
})(AccountForm);

InitializeFromStateForm = connect(
  state => ({
    initialValues: state.user.user
  }),
  {  }
)(InitializeFromStateForm);

export default InitializeFromStateForm;
