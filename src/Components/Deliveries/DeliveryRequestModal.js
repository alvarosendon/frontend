import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { addressShape } from '../../Shapes';
import AddressSelect from '../Address/AddressSelect';
import AddressItem from '../Address/AddressItem';

class DeliveryRequestModal extends React.PureComponent {

  renderSelectedAddress(selectedAddress) {

    if (selectedAddress === undefined || selectedAddress === null) {
      return null;
    }

    return (
      <div>
        <AddressItem item={selectedAddress} allowDelete={false} />
      </div>
    );
  }

  renderButtons() {

    const hasAddress = this.props.addresses !== undefined && this.props.addresses !== null && this.props.addresses.length !== 0;
    const hasSelectedAddress = this.props.selectedAddress !== undefined && this.props.selectedAddress !== null;

    if (hasAddress === false) {
      return (
        <Button color="primary" onClick={this.props.toggle}>
          <FormattedMessage id="documents.deliveryRequest.btn.close" />
        </Button>
      );
    }

    return (
      <Button color="primary" disabled={hasSelectedAddress === false} onClick={this.props.onRequest}>
        <FormattedMessage id="documents.deliveryRequest.btn.next" />
      </Button>
    );
  }

  render() {

    const { isOpen, toggle, onRequest, addresses, onAddressSelection, selectedAddress } = this.props;

    return (
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>
          <FormattedMessage id="documents.deliveryRequest.title" />
        </ModalHeader>
        <ModalBody>
          <p>
            <FormattedMessage id="documents.deliveryRequest.chooseAddress" />
          </p>
          {this.renderSelectedAddress(selectedAddress)}
          <AddressSelect addresses={addresses} onSelect={onAddressSelection} />
        </ModalBody>
        <ModalFooter>
          {this.renderButtons()}
        </ModalFooter>
      </Modal>
    );
  }

}

DeliveryRequestModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onRequest: PropTypes.func.isRequired,
  onAddressSelection: PropTypes.func.isRequired,
  addresses: PropTypes.arrayOf(addressShape).isRequired,
  selectedAddress: addressShape
}

export default DeliveryRequestModal;
