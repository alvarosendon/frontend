import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import Wrapper from '../../hoc/Wrapper';
import AddressDeleteConfirm from './AddressDeleteConfirm';

import './AddressItem.css'

class AddressItem extends React.PureComponent {

  renderButtons() {

    const editButton = (
      <a href="#editAddress" className="btn btn-secondary btn-sm btn-block" role="button" onClick={this.props.onEdit}>
        <FormattedMessage id="address.item.btn.edit" />
      </a>
    );

    if (this.props.allowDelete === true) {

      const deleteButton = (
        <button className="btn btn-secondary btn-sm btn-block" onClick={this.props.toggleConfirmDelete}>
          <FormattedMessage id="address.item.btn.delete" />
        </button>
      );

      return (
        <Wrapper>
          <div>{editButton}</div>
          <div>{deleteButton}</div>
        </Wrapper>
      );
    }

    return editButton;
  }

  render () {

    const {item, allowDelete } = this.props;
    let box = "";

    if (item.boxNumber !== null) {
      box = this.context.intl.formatMessage({id: "address.item.boxNumber"}, {boxNumber: item.boxNumber});
    }

    let deleteConfirm = null;

    if (allowDelete === true) {
      deleteConfirm = (
        <AddressDeleteConfirm
          isOpen={this.props.isOpenConfirmDelete}
          toggle={this.props.toggleConfirmDelete}
          onDelete={() => this.props.onDelete(this.props.item.id)}
          />
      );
    }

    return (
      <div className="card zc-card">
        <div className="card-body">
          <div className="row">
            <div className="col-sm-9">
              <p>{item.name} {item.surname}</p>
              <p>{item.street} {item.streetNumber} {box}</p>
              <p>{item.postalCode} {item.city}</p>
            </div>
            <div className="col-sm-3">
                {this.renderButtons()}
            </div>
          </div>
        </div>

        {deleteConfirm}

      </div>
    );
  }

}

AddressItem.contextTypes = {
    intl: intlShape.isRequired,
};

AddressItem.propTypes = {
  item: PropTypes.object.isRequired,
  onEdit: PropTypes.func.isRequired,
  allowDelete: PropTypes.bool.isRequired,
  onDelete: function(props, propName, componentName) {
    if (props.allowDelete === true && props[propName] === undefined) {
      return new Error(
        'Invalid prop `' + propName + '` supplied to' +
        ' `' + componentName + '`. Validation failed.'
      );
    }
  },
  isOpenConfirmDelete: function(props, propName, componentName) {
    if (props.allowDelete === true && props[propName] === undefined) {
      return new Error(
        'Invalid prop `' + propName + '` supplied to' +
        ' `' + componentName + '`. Validation failed.'
      );
    }
  },
  toggleConfirmDelete: function(props, propName, componentName) {
    if (props.allowDelete === true && props[propName] === undefined) {
      return new Error(
        'Invalid prop `' + propName + '` supplied to' +
        ' `' + componentName + '`. Validation failed.'
      );
    }
  },
};

AddressItem.defaultProps = {
  allowDelete: true
};

export default AddressItem;
