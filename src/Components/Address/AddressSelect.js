import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { addressShape } from '../../Shapes';

import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class AddressSelect extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    }

    this.toggle = this.toggle.bind(this);
  }

  toggle(e) {
    e.preventDefault();

    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {

    const {addresses, onSelect} = this.props;

    if (addresses === undefined || addresses === null || addresses.length === 0) {
      return (
        <div><FormattedMessage id="address.choose.empty" /></div>
      );
    }

    return (
      <Dropdown isOpen={this.state.isOpen} toggle={this.toggle}>
          <DropdownToggle caret>
            <FormattedMessage id="address.choose.title" />
          </DropdownToggle>
          <DropdownMenu>
            {addresses.map(address =>
              <DropdownItem key={address.id} onClick={() => onSelect(address)}>
                {address.street + " " + address.streetNumber + ", " + address.city}
              </DropdownItem>
            )}
          </DropdownMenu>
        </Dropdown>
    );
  }
}

AddressSelect.propTypes = {
  addresses: PropTypes.arrayOf(addressShape).isRequired,
  onSelect: PropTypes.func.isRequired
}

export default AddressSelect;
