import React from 'react';
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage, intlShape } from 'react-intl';
import { required, defaultMaxLength, maxLength } from '../../shared/validations';
import Input from '../../hoc/Input';

const maxLength3 = maxLength(3);
const maxLength10 = maxLength(10);
const maxLength16 = maxLength(16);
const maxLength64 = maxLength(64);
const maxLength120 = maxLength(120);

const AddressForm = (props) => {

  const { handleSubmit, submitting, intl } = props;

  return (
    <form onSubmit={handleSubmit} autoComplete="off">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-8">
            <Field id="txtAddressName" name="name" type="text" className="form-control" maxLength="120"
                component={Input}
                label={intl.formatMessage({id: "address.field.name"})}
                placeholder={intl.formatMessage({id: "address.field.name.placeholder"})}
                validate={[ required, maxLength120 ]}
              />
          </div>
        </div>

        <div className="row">
          <div className="col-md-8">
            <Field id="txtAddressSurname" name="surname" type="text" className="form-control" maxLength="120"
                component={Input}
                label={intl.formatMessage({id: "address.field.surname"})}
                placeholder={intl.formatMessage({id: "address.field.surname.placeholder"})}
                validate={[ required, maxLength120 ]}
              />
          </div>
        </div>

        <div className="row">
          <div className="col-md-8">
            <Field id="txtAddressStreetName" name="street" type="text" className="form-control" maxLength="255"
                component={Input}
                label={intl.formatMessage({id: "address.field.streetName"})}
                placeholder={intl.formatMessage({id: "address.field.streetName.placeholder"})}
                validate={[ required, defaultMaxLength ]}
              />
            </div>
          <div className="col-md-2">
            <Field id="txtAddressStreetNumber" name="streetNumber" type="text" className="form-control" maxLength="16"
                component={Input}
                label={intl.formatMessage({id: "address.field.streetNumber"})}
                placeholder={intl.formatMessage({id: "address.field.streetNumber.placeholder"})}
                validate={[ required, maxLength16 ]}
              />
          </div>
          <div className="col-md-2">
            <Field id="txtAddressBoxNumber" name="boxNumber" type="text" className="form-control" maxLength="3"
                component={Input}
                label={intl.formatMessage({id: "address.field.boxNumber"})}
                placeholder={intl.formatMessage({id: "address.field.boxNumber.placeholder"})}
                validate={[ maxLength3 ]}
              />
          </div>
        </div>

        <div className="row">
          <div className="col-md-4">
            <Field id="txtAddressPostalCode" name="postalCode" type="text" className="form-control" maxLength="10"
                component={Input}
                label={intl.formatMessage({id: "address.field.postalCode"})}
                placeholder={intl.formatMessage({id: "address.field.postalCode.placeholder"})}
                validate={[ required, maxLength10 ]}
              />
          </div>
          <div className="col-md-4">
            <Field id="txtAddressCity" name="city" type="text" className="form-control" maxLength="64"
                component={Input}
                label={intl.formatMessage({id: "address.field.city"})}
                placeholder={intl.formatMessage({id: "address.field.city.placeholder"})}
                validate={[ required, maxLength64 ]}
              />
          </div>
        </div>
      </div>
    </form>
  );

}

AddressForm.propTypes = {
    intl: intlShape.isRequired,
};

let InitializeFromStateForm = reduxForm({
  form: 'addressForm'
})(AddressForm);

InitializeFromStateForm = connect(
  state => ({
    initialValues: state.address.item
  }),
  {  }
)(InitializeFromStateForm);

export default InitializeFromStateForm;
