import React from 'react';

import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AddressForm from './AddressForm';

import './AddressModalForm.css';

const AddressModalForm = ({intl, isOpen, toggle, onSubmit, onSaveClick, isNew }) => {

  return (
    <Modal isOpen={isOpen} toggle={toggle} id="addressFormModal">
      <ModalHeader toggle={toggle} tag="h4">
        <FormattedMessage id={isNew?"address.create.title":"address.edit.title"} />
      </ModalHeader>
      <ModalBody>
        <AddressForm intl={intl} onSubmit={onSubmit} allowBack={false} />
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-primary btn-md" onClick={onSaveClick}>
          <FormattedMessage id="address.btn.save"/>
        </button>
      </ModalFooter>
    </Modal>
  );

};

AddressModalForm.propTypes = {
  intl: intlShape.isRequired,
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSaveClick: PropTypes.func.isRequired,
  isNew: PropTypes.bool.isRequired
}

export default AddressModalForm;
