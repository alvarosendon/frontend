import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { GlobalError } from '../Components/GlobalError';
import CreateAccountForm from '../Components/CreateAccountForm';
import { submit } from 'redux-form';
import { userCreateAccount } from '../store/actions';

class CreateAccountModal extends React.Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCreateClick = this.handleCreateClick.bind(this);
  }

  componentDidUpdate(prevProps, prevState){

    if (this.props.isOpen === true && prevProps.isOpen === false) {
        this.emailInput.focus();
    }
  }

  handleCreateClick() {
    this.props.submit("createAccountForm");
  }

  handleSubmit(model) {

    this.props.userCreateAccount(model.email, model.password, model.passwordRepeat, this.context.intl);
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle}>
        <ModalHeader toggle={this.toggleModal} tag="h4">
          <FormattedMessage id="createAccount.title" />
        </ModalHeader>
        <ModalBody>
          <GlobalError error={this.props.error} />
          <div>
            <FormattedMessage id="createAccount.description" />
          </div>
          <CreateAccountForm
            intl={this.context.intl}
            firstInputRef={node => this.emailInput = node}
            onSubmit={this.handleSubmit}
          />
        </ModalBody>
        <ModalFooter>
          <button className="btn btn-primary"
            onClick={this.handleCreateClick}
             disabled={this.props.loading}>
            <FormattedMessage id="createAccount.button" />
          </button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateAccountModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
};

CreateAccountModal.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    loggedIn: state.security.loggedIn,
    error: state.user.createAccount.error,
    loading: state.user.createAccount.loading
  };
}

const mapDispatchToProps = { submit, userCreateAccount }

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccountModal);
