import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { GlobalError } from '../Components/GlobalError';
import LoginForm from '../Components/LoginForm';
import { submit } from 'redux-form';

class LoginModal extends React.Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleLogInClick = this.handleLogInClick.bind(this);
    this.handleForgotPassword = this.handleForgotPassword.bind(this);
    this.handleCreateAccount = this.handleCreateAccount.bind(this);
  }

  componentDidUpdate(prevProps, prevState){

    if (this.props.isOpen === true && prevProps.isOpen === false) {
      this.usernameInput.focus();
    }
  }

  handleSubmit(model) {

    this.props.onLogIn(model.email, model.password);
  }

  handleLogInClick() {
    this.props.submit("loginForm");
  }

  handleForgotPassword(e) {
    e.preventDefault();

    this.props.toggle();
    this.props.onForgotPassword();
  }

  handleCreateAccount(e) {

    e.preventDefault();

    this.props.toggle();
    this.props.onCreateAccount();
  }

  render() {

    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} id="logInForm">
        <ModalHeader toggle={this.props.toggle} tag="h4">
          <FormattedMessage id="login.title" />
        </ModalHeader>
        <ModalBody>

          <GlobalError error={this.props.error} />
          <LoginForm
            intl={this.context.intl}
            firstInputRef={node => this.usernameInput = node}
            handleForgotPassword={this.handleForgotPassword}
            onSubmit={this.handleSubmit} />

          <div>
            <FormattedMessage id="login.createAccount"/>&nbsp;
            <a href="#createAccount"
              onClick={this.handleCreateAccount}>
              <FormattedMessage id="login.createAccount.link"/>
            </a>
          </div>

        </ModalBody>
        <ModalFooter>
          <button className="btn btn-primary" onClick={this.handleLogInClick} disabled={this.props.loading}>
            <FormattedMessage id="login.btn" />
          </button>
        </ModalFooter>
      </Modal>
    );
  }

}

LoginModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onLogIn: PropTypes.func.isRequired,
  onCreateAccount: PropTypes.func.isRequired,
  onForgotPassword: PropTypes.func.isRequired
};

LoginModal.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    error: state.security.error,
    loading: state.security.loading
  };
}

const mapDispatchToProps = { submit }

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
