import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { GlobalError } from '../Components/GlobalError';
import ForgotPasswordForm from '../Components/ForgotPasswordForm';
import { submit } from 'redux-form';

import { userForgotPassword } from '../store/actions';

class ForgotPasswordModal extends React.Component {

  constructor(props) {
    super(props);

    this.handleForgotClick = this.handleForgotClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps, prevState){

    if (this.props.isOpen === true && prevProps.isOpen === false) {
        this.emailInput.focus();
    }
  }

  handleForgotClick() {
    this.props.submit("forgotPasswordForm");
  }

  handleSubmit(model) {
    this.props.userForgotPassword(model.email, this.context.intl);
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle}>
        <ModalHeader toggle={this.toggleModal} tag="h4">
          <FormattedMessage id="forgotPassword.title" />
        </ModalHeader>
        <ModalBody>
          <GlobalError error={this.props.error} />
          <FormattedMessage id="forgotPassword.description" />
          <ForgotPasswordForm
            intl={this.context.intl}
            firstInputRef={node => this.emailInput = node}
            onSubmit={this.handleSubmit}
            />
        </ModalBody>
        <ModalFooter>
          <button className="btn btn-primary"
            onClick={this.handleForgotClick}
             disabled={this.props.loading}>
            <FormattedMessage id="forgotPassword.button" />
          </button>
        </ModalFooter>
      </Modal>
    );
  }
}

ForgotPasswordModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
};

ForgotPasswordModal.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    loggedIn: state.security.loggedIn,
    error: state.user.forgotPassword.error,
    loading: state.user.forgotPassword.loading
  };
}

const mapDispatchToProps = { submit, userForgotPassword }

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordModal);
