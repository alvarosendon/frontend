import axios from 'axios';

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    common: {
      "Accept": "application/json",
      "Client": "react-application"
    }
  },
  post: {
    'Content-Type': 'application/json'
  },
  put: {
    'Content-Type': 'application/json'
  }
});

export default api;
