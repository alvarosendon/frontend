import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { intlShape } from 'react-intl';
import Select from 'react-select';
import { fieldShape } from '../Shapes';

import './Field.css'

class Field extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      values: [],
      maxChoicesError: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOptions) => {
    const maxChoices = this.props.field.maxChoices;

    if (selectedOptions !== null && selectedOptions.length > maxChoices) {
      this.setState({maxChoicesError: true});
    } else {

      this.setState({values: selectedOptions, maxChoicesError: false});

      this.props.onChangeHandler(selectedOptions, this.props.field.id);
    }
  }

  render () {

    const value = this.state.values;
    const maxChoicesError = this.state.maxChoicesError;
    const field = this.props.field;
    const multiple = field.maxChoices > 1;
    const showError = this.state.maxChoicesError;

    let errorText = "";

    if (maxChoicesError) {
      errorText = this.context.intl.formatMessage({id: "validations.maxChooices"}, {maxChoices: field.maxChoices});
    }

    let label = field.labelEn;

    const lang = this.props.locale.lang[0].toUpperCase() + this.props.locale.lang.substring(1);

    if (field.hasOwnProperty(`label${lang}`)) {
      label = field[`label${lang}`];
    }

    return (
      <div className="form-group">
        <label >{label}</label>
        <Select
          options={field.choices}
          onChange={this.handleChange}
          closeMenuOnSelect={true}
          getOptionValue={(option) => option.id}
          getOptionLabel={(option) => option.labelEn}
          value={value}
          isMulti={multiple}
          isClearable
          isSearchable={true}
        />
        <div className={showError?"":"d-none"}>
          <small className="text-danger">{errorText}</small>
        </div>
      </div>
    );
  }

}

Field.propTypes = {
  onChangeHandler: PropTypes.func.isRequired,
  field: fieldShape.isRequired
};

Field.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    "locale": state.locale
  };
}

const mapDispatchToProps = { }

export default connect(mapStateToProps, mapDispatchToProps)(Field);
