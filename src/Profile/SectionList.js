import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { connect } from "react-redux";
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { nextSection } from '../store/actions';

import Section from './Section'

import './SectionList.css'

class SectionList extends React.Component {

  render () {

    const { error, loading, list, currentSection, opened } = this.props;

    let content;

    if (loading) {
      content = (
        <div><FormattedMessage id="profile.sections.loading" /></div>
      );
    } else if (error) {
      content = (
        <div><FormattedMessage id="profile.sections.error" /></div>
      );
    } else if (!list.length) {
      content = (
        <div><FormattedMessage id="profile.filled" /></div>
      );
    } else if (!opened) {
      return null;
    } else {
      content = (
        list.map((s, index) =>
          <Section section={s} index={index} currentIndex={currentSection} totalSections={list.length} key={s.id} />
        )
      );
    }

    return (
      <Modal isOpen={opened} toggle={this.props.toggle}>
        <ModalHeader>
          <FormattedMessage id="profile.title" />
        </ModalHeader>
        <ModalBody>
            {content}
        </ModalBody>
      </Modal>
    );
  }

}

SectionList.propTypes = {
  opened: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
}

SectionList.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    list: state.profile.list,
    loading: state.profile.loading,
    error: state.profile.error,
    currentSection: state.profile.currentSection,
    opened: state.profile.opened
  };
}

const mapDispatchToProps = { nextSection }

export default connect(mapStateToProps, mapDispatchToProps)(SectionList);
