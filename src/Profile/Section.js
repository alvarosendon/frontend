import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { FormattedMessage, intlShape } from 'react-intl';
import { saveProfile } from '../store/actions';
import { sectionShape } from '../Shapes';
import Field from './Field'

class Section extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      fieldsValues: {},
      fieldError: false
    };

    this.handleNextClick = this.handleNextClick.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.validateFields = this.validateFields.bind(this);
  }

  handleNextClick(e) {
    e.preventDefault();

    if (this.validateFields()) {

      const finalStep = this.isFinalStep();

      this.props.saveProfile(this.props.section.id, this.state.fieldsValues, finalStep);
    }
  }

  validateFields() {

    const fieldsIds = Object.keys(this.state.fieldsValues);

    // Check if all the fields has at least one selection
    if (this.props.section.fields.length !== fieldsIds.length) {

      this.setState({ fieldError: true});

      return false;
    }

    this.setState({ fieldError: false});

    return true;
  }

  isFinalStep() {
    return this.props.index === this.props.totalSections - 1;
  }

  handleFieldChange = (newFieldValue, fieldId) => {

    if (( typeof newFieldValue === "string" && newFieldValue === "")
          || (Array.isArray(newFieldValue) && newFieldValue.length === 0)
          || newFieldValue === undefined || newFieldValue === null) {
      delete this.state.fieldsValues[fieldId];
    } else {
      this.setState(prevState => ({
          fieldsValues: {
              ...prevState.fieldsValues,
              [fieldId]: newFieldValue.length?newFieldValue.map(v => v.id):[newFieldValue.id]
          }
      }));
    }
  }

  render () {

    const { section, index, currentIndex, totalSections } = this.props;

    const active = index === currentIndex;
    const finalStep = this.isFinalStep();

    if (!active) {
      return null;
    }

    let buttonKey;
    if (finalStep) {
      buttonKey = "profile.section.btn.finish";
    } else {
      buttonKey = "profile.section.btn.next";
    }

    return (
      <form>
        <h5>{section.labelEn}</h5>
        <div className={this.state.fieldError?"alert alert-danger":"d-none"}>
          <FormattedMessage id="profile.section.error.fillFields"/>
        </div>
        {section.fields.map(f =>
          <Field field={f} key={f.id} onChangeHandler={this.handleFieldChange}/>
        )}
        <div className="modal-footer">
          <div className="w-100">
            <FormattedMessage id="profile.section.step" values={{current: index + 1, total: totalSections}} />
          </div>
          <button type="button" className="btn btn-sm btn-primary"
            onClick={this.handleNextClick}>
              <FormattedMessage id={buttonKey} />
            </button>
        </div>
      </form>
    );
  }

}

Section.propTypes = {
  index: PropTypes.number.isRequired,
  currentIndex: PropTypes.number.isRequired,
  totalSections: PropTypes.number.isRequired,
  section: sectionShape.isRequired
};

Section.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {

  };
}

const mapDispatchToProps = { saveProfile }

export default connect(mapStateToProps, mapDispatchToProps)(Section);
