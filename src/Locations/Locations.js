import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Leaflet from 'leaflet'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import Wrapper from '../hoc/Wrapper';
import LocationFilter from './LocationFilter'
import { fetchLocations } from '../store/actions';

import './Locations.css'

Leaflet.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/images/'

const markerIcon = new Leaflet.Icon({
                       iconUrl: require('../images/map-location.svg'),
                       iconSize: [32, 52]
                   })

// Brussels
const defaultPosition = [50.859017, 4.343561];
const defaultZoom = 8;
const cityZoom = 11;

class Locations extends React.Component {

  constructor(props) {
    super(props);

    this.state = {

      position: defaultPosition,
      zoom: defaultZoom
    }

    this.changePosition = this.changePosition.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentWillMount() {

    this.props.fetchLocations();
  }

  /**
  * Handle city filtering to do zoom to the selected city.
  */
  handleFilter(city) {

    if (city === undefined || city === null) {
      this.changePosition(defaultPosition[0], defaultPosition[1], defaultZoom);
    } else {
      this.changePosition(city.latitude, city.longitude, city.zoom?city.zoom:cityZoom);
    }
  }

  /**
  * Change the position of the map to do zoom to the selected coordinates.
  */
  changePosition(latitude, longitude, zoom) {

    if (zoom === undefined) {
      zoom = defaultZoom;
    }
    this.setState({"position": [latitude, longitude], "zoom": zoom});
  }

  render () {

    // Try to use brower location API, if it's available, to move the map from default location to user location.
    /*if ("geolocation" in navigator) {

      const changePos = this.changePosition;
      navigator.geolocation.getCurrentPosition(
        (position) => changePos(position.coords.latitude, position.coords.longitude),
        (error) => console.log(error)
      );
    }*/

    return (
      <Wrapper>
        <div className="row locations no-gutters">

          <div className="col-md-4 pt-2 pl-3 pr-3">
            <LocationFilter onFilter={this.handleFilter}/>
          </div>
          <div className="col-md-8 mr-4-neg">
            <Map center={this.state.position} zoom={this.state.zoom} style={{"height": "100%", "minHeight": "200px"}}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
              />
              {this.props.locations.filter(l => l.latitude !== undefined && l.latitude !== null).map(l =>
                <Marker key={"locationMarker" + l.id} position={[l.latitude, l.longitude]} icon={markerIcon}>
                  <Popup>
                    <div><strong>{l.name}</strong></div>
                    <div>{l.address}</div>
                    {l.openingHours !== ""?<div>Opening hours: {l.openingHours}</div>:null}
                  </Popup>
                </Marker>
              )}

            </Map>
          </div>
        </div>
      </Wrapper>
    );
  }

}

Locations.propTypes = {
  locations: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    locations: state.locations.locations
  };
}

const mapDispatchToProps = { fetchLocations }

export default connect(mapStateToProps, mapDispatchToProps)(Locations);
