import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import SelectAsync from 'react-select/lib/Async';
import { FormattedMessage, intlShape } from 'react-intl';
import GeonamesApi from '../GeonamesApi';
import { fetchCities } from '../store/actions';
import './LocationFilter.css';

const geoNamesFilter = "geoNameId_";

class LocationFilter extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: -1
    }

    this.handleOnSelect = this.handleOnSelect.bind(this);
    this.renderFilterItem = this.renderFilterItem.bind(this);
    this.filter = this.filter.bind(this);
    this.loadOptions = this.loadOptions.bind(this);

    this.geonamesApi = new GeonamesApi("zerocopy", "BE");
  }

  componentWillMount() {

    this.props.fetchCities();
  }

  filter(inputValue: string) {

    return this.props.cities.filter(city => {

      const postalCodes = city.postalCodes;
      const name = city.name;
      const hasName = name.toLowerCase().startsWith(inputValue.toLowerCase());

      if (postalCodes && postalCodes.length > 0) {
        return hasName || postalCodes.filter(postalCode => postalCode.startsWith(inputValue)).length > 0;
      }

      return hasName;
    });
  }

  loadOptions(inputValue, callback) {

    const filterCities = this.filter(inputValue);

    if (filterCities.length === 0 && inputValue.length >= 3) {

      this.geonamesApi.cities(inputValue).then(resp => {

        if (resp.data.geonames) {

          const newOptions = resp.data.geonames.map(g => {
            return {
              id: geoNamesFilter + g.geonameId,
              name: g.name,
              latitude: g.lat,
              longitude: g.lng
            }
          });

           callback(newOptions);

        } else {
          callback([]);
        }

      }).catch(error => {
        callback([]);
      });

    } else {
      callback(filterCities);
    }
  }

  handleOnSelect(selected) {

    let newOption = selected;

    if (selected === undefined || selected === null) {
      newOption = {id: -1};
    }

    if (this.state.selected !== newOption.id) {
        this.setState({selected: newOption.id});
    }

    if (typeof newOption.id === "string" && newOption.id.startsWith(geoNamesFilter)) {
      this.props.onFilter(newOption);
    } else {
      const filterCities = this.props.cities.filter(c => c.id === newOption.id);
      const selectedCity =  filterCities.length === 0?undefined:filterCities[0];

      this.props.onFilter(selectedCity);
    }
  }

  renderFilterItem(city) {
    let id = -1;
    let name = <FormattedMessage id="locations.filter.all" />;

    if (city !== undefined) {
      id = city.id;
      name = city.name;
    }

    return (
      <div key={"cityFilter" + id}>
        <span className={this.state.selected === id?"filter-item active":"filter-item"} onClick={() => this.handleOnSelect(city)}>
          {name}
        </span>
      </div>
    );
  }

  render () {

    const sortedCities = this.props.cities.sort(function(a, b){
      if(a.name < b.name) return -1;
      if(a.name > b.name) return 1;
      return 0;
    });

    const middle = sortedCities.length / 2;

    const fistColumnCities = sortedCities.slice(0, middle);
    const secondColumnCities = sortedCities.slice(middle);

    return (
      <div className="LocationFilter">
        <h3><FormattedMessage id="locations.filter.title" /></h3>
        <div>
          <FormattedMessage id="locations.filter.description" />
        </div>

         <div className="mt-3">

           <SelectAsync
             cacheOptions
             loadOptions={this.loadOptions}
             defaultOptions={this.props.cities}
             closeMenuOnSelect={true}
             openMenuOnFocus={false}
             openMenuOnClick={false}
             isClearable
             getOptionValue={(option) => (`${option.id}`)}
             getOptionLabel={(option) => (`${option.name}`)}
             placeholder={this.context.intl.formatMessage({id: 'locations.filter.field'})}
             noOptionsMessage={() => (this.context.intl.formatMessage({id: 'locations.filter.empty'}))}
             onChange={this.handleOnSelect}
          />

         </div>

         <div className="row mt-3">
           <div className="col-6">
                {this.renderFilterItem()}
                {fistColumnCities.map(c => this.renderFilterItem(c))}
            </div>
            <div className="col-6">
                 {secondColumnCities.map(c => this.renderFilterItem(c))}
             </div>
         </div>
      </div>
    );
  }
}

LocationFilter.propTypes = {
  onFilter: PropTypes.func.isRequired,
  cities: PropTypes.array.isRequired
};

LocationFilter.contextTypes = {
    intl: intlShape.isRequired,
};

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    cities: state.locations.cities
  };
}

const mapDispatchToProps = { fetchCities }

export default connect(mapStateToProps, mapDispatchToProps)(LocationFilter);
