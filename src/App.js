import React, { Component,  } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { IntlProvider } from 'react-intl';
import { Switch, Route } from 'react-router-dom';
import {Helmet} from "react-helmet";

import Notifications from 'react-notification-system-redux';

import Billboard from './Commons/Billboard';
import Toolbar from './Components/Toolbar';
import EarnCredits from './Components/Credits/EarnCredits';
import StudentAbout from './Commons/StudentAbout';
import MediaAbout from './Commons/MediaAbout';
import Footer from './Commons/Footer';
import Locations from './Locations/Locations';
import { NoMatch } from './Components/NoMatch';

import { PrivateRoute } from './Commons/PrivateRoute';
import DocumentList from './Containers/DocumentList';
import AddressList from './Containers/AddressList';
import Privacy from './Containers/Privacy';
import Stats from './Containers/Stats';
import Account from './Containers/Account';
import Ref from './Containers/Ref';

import { getToken } from './shared/utils';

import { directLogIn } from './store/actions';

import './App.css';

let ConnectedIntlProvider = connect(mapStateToProps)(IntlProvider)

class App extends Component {

  componentDidMount() {
    //this.context.mixpanel.track('Test app react.');

    const token = getToken();

    if (token !== undefined && token !== null && token !== "") {

      if (this.props.loggedIn === false) {
        this.props.directLogIn();
      }

    }
  }

  renderAbout() {

    if (this.props.loggedIn === true) {
      return null;
    }

    return (
      <div className="container-fluid content pb-3 pt-3">
        <div className="row">
          <div className="col-md-6">
            <StudentAbout />
          </div>
          <div className="col-md-6">
            <MediaAbout />
          </div>
        </div>
      </div>
    );
  }


  render() {

    return (
      <ConnectedIntlProvider >
          <div className="App">

            <Helmet>
              <meta property="og:url"           content={this.props.location.pathname} />
              <meta property="og:type"          content="website" />
              <meta property="og:title"         content="Share Zerocopy with your friends!" />
              <meta property="og:description"   content="Hello, You have to check out Zerocopy, they let students (like us) print for free! It's awesome :-)" />
              <meta property="og:image"         content="" />
            </Helmet>

            <Notifications
              notifications={this.props.notifications}
              style={false}
            />
              <Billboard show={this.props.loggedIn}/>
              <Toolbar />
              <div className="container-fluid content main-content">
                <div className="row pb-3 no-gutters">
                  <div className={this.props.loggedIn === true?"col-md-8 mt-4":"col-md-12"}>
                    <Switch>
                      <Route exact path='/' component={DocumentList} />
                      <Route path="/ref/:ref_id" component={Ref} />
                      <PrivateRoute exact path='/user/addresses' component={AddressList} />
                      <PrivateRoute exact path='/user/privacy' component={Privacy} />
                      <PrivateRoute exact path='/user/stats' component={Stats} />
                      <PrivateRoute exact path='/user/account' component={Account} />
                      <Route component={(props) => <NoMatch {...props} loggedIn={this.props.loggedIn} />} />
                    </Switch>
                  </div>
                  <div className={this.props.loggedIn === true?"col-md-4 mt-4 pr-2 pl-2 pl-md-0":""}>
                    <EarnCredits />
                  </div>
                </div>

              </div>
              <Locations />
              {this.renderAbout()}
              <Footer />
          </div>
      </ConnectedIntlProvider>
    );
  }
}

App.propTypes = {
  location: PropTypes.object.isRequired
}

App.contextTypes = {
    mixpanel: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    locale: state.locale.lang,
    messages: state.locale.messages,
    loggedIn: state.security.loggedIn,
    notifications: state.notifications
  };
}

const mapDispatchToProps = { directLogIn }

export default connect(mapStateToProps, mapDispatchToProps)(App);
