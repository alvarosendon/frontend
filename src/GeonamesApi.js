import axios from 'axios';

class GeonamesApi {

  constructor(username, country) {

    this.api = axios.create({
      baseURL: process.env.REACT_APP_GEONAMES_API,
    });

    this.config = {
      username: username,
      country: country,
      encoding: "JSON"
    }
  }

  cities(queryString) {

    const requestConf = {...this.config, ...{q: queryString}, ...{featureClass: "P"}}
    const fullApiName = `search${requestConf.encoding}`

    return this.api.get(fullApiName, {
      params: requestConf
    });
  }
}

export default GeonamesApi;
